-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 15, 2020 at 04:23 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `assesment`
--

-- --------------------------------------------------------

--
-- Table structure for table `customer_tbl`
--

CREATE TABLE `customer_tbl` (
  `ct_id` int(255) NOT NULL,
  `ct_id_type` varchar(255) CHARACTER SET utf8 NOT NULL,
  `ct_id_number` varchar(255) CHARACTER SET utf8 NOT NULL,
  `ct_display_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `ct_dob` varchar(255) CHARACTER SET utf8 NOT NULL,
  `ct_email` varchar(255) CHARACTER SET utf8 NOT NULL,
  `ct_contact_id` varchar(255) CHARACTER SET utf8 NOT NULL,
  `ct_contact_type` varchar(255) CHARACTER SET utf8 NOT NULL,
  `ct_contact_value` varchar(30) CHARACTER SET utf8 NOT NULL,
  `ct_roles_code` varchar(255) CHARACTER SET utf8 NOT NULL,
  `ct_roles_desc` varchar(255) CHARACTER SET utf8 NOT NULL,
  `ct_nationality` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `ct_race` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `ct_address_id` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `ct_primaryAddress` tinyint(1) DEFAULT NULL,
  `ct_line1` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `ct_line2` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `ct_line3` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `ct_postalCode` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `ct_city` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `ct_state` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `ct_country` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `ct_param1` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `ct_param2` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `ct_param3` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `ct_param4` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `ct_param5` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `ct_created_datetime` datetime NOT NULL DEFAULT current_timestamp(),
  `ct_updated_datetime` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer_tbl`
--

INSERT INTO `customer_tbl` (`ct_id`, `ct_id_type`, `ct_id_number`, `ct_display_name`, `ct_dob`, `ct_email`, `ct_contact_id`, `ct_contact_type`, `ct_contact_value`, `ct_roles_code`, `ct_roles_desc`, `ct_nationality`, `ct_race`, `ct_address_id`, `ct_primaryAddress`, `ct_line1`, `ct_line2`, `ct_line3`, `ct_postalCode`, `ct_city`, `ct_state`, `ct_country`, `ct_param1`, `ct_param2`, `ct_param3`, `ct_param4`, `ct_param5`, `ct_created_datetime`, `ct_updated_datetime`) VALUES
(1001, 'NEWNRIC', '900101-10-9943', 'John Doe', '1990-01-01', 'test@test.com', '10', 'PHONE', '60123456789', 'PROFILE', 'My Profile', NULL, NULL, '5', 1, '18 JALAN BUKIT NANAS', NULL, NULL, '50250', 'KUALA LUMPUR', 'W.P. KUALA LUMPUR', 'MALAYSIA', 'Mr', 'MALE', 'EMAIL', NULL, NULL, '2020-05-12 20:54:57', '2020-05-15 22:03:21');

-- --------------------------------------------------------

--
-- Table structure for table `orders_tbl`
--

CREATE TABLE `orders_tbl` (
  `ot_id` int(255) NOT NULL,
  `ot_trx_ref_id` int(255) NOT NULL,
  `ot_status` varchar(255) CHARACTER SET utf8 NOT NULL,
  `ot_status_desc` varchar(255) CHARACTER SET utf8 NOT NULL,
  `ot_remarks` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `ot_param1` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT 'customer_id',
  `ot_param2` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `ot_param3` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `ot_param4` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `ot_param5` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `ot_created_datetime` datetime NOT NULL DEFAULT current_timestamp(),
  `ot_updated_datetime` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders_tbl`
--

INSERT INTO `orders_tbl` (`ot_id`, `ot_trx_ref_id`, `ot_status`, `ot_status_desc`, `ot_remarks`, `ot_param1`, `ot_param2`, `ot_param3`, `ot_param4`, `ot_param5`, `ot_created_datetime`, `ot_updated_datetime`) VALUES
(1, 1000, 'COMPLETED', 'Complete', NULL, '1001', NULL, NULL, NULL, NULL, '2020-05-13 21:28:05', '2020-05-13 22:02:27'),
(2, 1001, 'EXCEPTION', 'Unsuccessful', 'Fraud investigation in progress', '1001', NULL, NULL, NULL, NULL, '2020-05-13 21:28:05', '2020-05-13 22:02:30');

-- --------------------------------------------------------

--
-- Table structure for table `role_tbl`
--

CREATE TABLE `role_tbl` (
  `rt_id` int(255) NOT NULL,
  `rt_ct_id` int(255) NOT NULL,
  `rt_code` varchar(255) CHARACTER SET utf8 NOT NULL,
  `rt_description` varchar(255) CHARACTER SET utf8 NOT NULL,
  `rt_param1` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `rt_param2` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `rt_param3` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `rt_param4` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `rt_param5` varchar(255) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role_tbl`
--

INSERT INTO `role_tbl` (`rt_id`, `rt_ct_id`, `rt_code`, `rt_description`, `rt_param1`, `rt_param2`, `rt_param3`, `rt_param4`, `rt_param5`) VALUES
(1, 1001, 'PROFILE', 'My Profile', NULL, NULL, NULL, NULL, NULL),
(2, 1001, 'TRANSACTION', 'TRANSACTION', NULL, NULL, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customer_tbl`
--
ALTER TABLE `customer_tbl`
  ADD PRIMARY KEY (`ct_id`);

--
-- Indexes for table `orders_tbl`
--
ALTER TABLE `orders_tbl`
  ADD PRIMARY KEY (`ot_id`);

--
-- Indexes for table `role_tbl`
--
ALTER TABLE `role_tbl`
  ADD PRIMARY KEY (`rt_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customer_tbl`
--
ALTER TABLE `customer_tbl`
  MODIFY `ct_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1002;

--
-- AUTO_INCREMENT for table `orders_tbl`
--
ALTER TABLE `orders_tbl`
  MODIFY `ot_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `role_tbl`
--
ALTER TABLE `role_tbl`
  MODIFY `rt_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
