package com.test.helper;

import com.test.bean.RequestResult;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class ControllerHelper {

    public static ResponseEntity<RequestResult> standardize(RequestResult requestResult){
        return requestResult.getMeta() != null ? new ResponseEntity<RequestResult>(requestResult, HttpStatus.OK) : new ResponseEntity<RequestResult>(requestResult, HttpStatus.BAD_REQUEST);
    }

    public ResponseEntity<RequestResult> standardizeValidationFailed(String message){
        RequestResult requestResult = new RequestResult();
        requestResult.setContent(message);
        return requestResult.getMeta() != null ? new ResponseEntity<RequestResult>(requestResult, HttpStatus.OK) : new ResponseEntity<RequestResult>(requestResult, HttpStatus.BAD_REQUEST);
    }
}
