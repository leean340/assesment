package com.test.helper;

import com.test.bean.RequestResult;
import com.test.util.ErrorCode;
import com.test.util.SuccessCode;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class RequestResultHelper {

    public static RequestResult standardize(boolean isSuccess, String errorMessage) {
        RequestResult requestResult = new RequestResult();

        if (!isSuccess) {
            requestResult.setMeta("");
            requestResult.setContent(errorMessage);
        } else {
            requestResult.setMeta("");
            requestResult.setContent(SuccessCode.SUCCESS_CODE_MSG);
        }

        return requestResult;
    }

    public static RequestResult standardizeWithObject(Object object, Object meta) {
        RequestResult requestResult = new RequestResult();

        requestResult.setMeta(meta);
        requestResult.setContent(object);
//        if (object == null) {
//            requestResult.setResponseCode(ErrorCode.ERROR_CODE);
//            requestResult.setMsg(ErrorCode.ERROR_CODE_MSG);
//            requestResult.setContent(errorMessage);
//        } else {
//            Optional<Object> optional = Optional.of(object);
//            requestResult.setContent(optional.orElseGet(Object::new));
//        }

        return requestResult;
    }

}
