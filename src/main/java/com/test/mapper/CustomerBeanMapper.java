package com.test.mapper;

import com.test.bean.CustomerBean;
import com.test.bean.CustomerBeanExample;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustomerBeanMapper {
    int countByExample(CustomerBeanExample example);

    int deleteByExample(CustomerBeanExample example);

    int deleteByPrimaryKey(Integer ctId);

    int insert(CustomerBean record);

    int insertSelective(CustomerBean record);

    List<CustomerBean> selectByExample(CustomerBeanExample example);

    CustomerBean selectByPrimaryKey(Integer ctId);

    int updateByExampleSelective(@Param("record") CustomerBean record, @Param("example") CustomerBeanExample example);

    int updateByExample(@Param("record") CustomerBean record, @Param("example") CustomerBeanExample example);

    int updateByPrimaryKeySelective(CustomerBean record);

    int updateByPrimaryKey(CustomerBean record);
}