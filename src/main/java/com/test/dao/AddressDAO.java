package com.test.dao;

import lombok.Data;

@Data
public class AddressDAO {
    String id;

    boolean primaryAddress;

    String line1;

    String line2;

    String line3;

    String postalCode;

    String city;

    String state;

    String country;
}
