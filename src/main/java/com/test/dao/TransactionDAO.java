package com.test.dao;

import lombok.Data;

import java.util.Date;

@Data
public class TransactionDAO {
    int trxRefId;

    String status;

    String statusDesc;

    String remarks;

    Date created;

    Date lastUpdated;
}
