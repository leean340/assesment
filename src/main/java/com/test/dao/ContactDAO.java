package com.test.dao;

import lombok.Data;

@Data
public class ContactDAO {
    String id;

    String type;

    String value;
}
