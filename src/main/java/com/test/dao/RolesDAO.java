package com.test.dao;

import lombok.Data;

@Data
public class RolesDAO {
    String code;

    String description;
}
