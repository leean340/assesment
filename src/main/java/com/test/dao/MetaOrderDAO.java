package com.test.dao;

import lombok.Data;

@Data
public class MetaOrderDAO {
    String code;

    String readCount;
}
