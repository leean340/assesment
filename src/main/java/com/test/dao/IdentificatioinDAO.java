package com.test.dao;

import lombok.Data;

@Data
public class IdentificatioinDAO {
    String idType;

    String idNumber;
}
