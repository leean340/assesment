package com.test.dao;

import lombok.Data;

import java.util.List;

@Data
public class CustomerDAO {
    String id;

    IdentificatioinDAO identificatioinDAO;

    DetailsDAO detailsDAO;

    List<ContactDAO> contactDAOList;

    List<RolesDAO> rolesDAOList;
}
