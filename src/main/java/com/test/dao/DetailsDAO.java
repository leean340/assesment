package com.test.dao;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class DetailsDAO {
    String salutation;

    String displayName;

    String gender;

    String dateOfBirth;

    String email;

    String nationality;

    String race;

    List<AddressDAO> addressDAOList = new ArrayList<>();
}
