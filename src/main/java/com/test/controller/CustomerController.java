package com.test.controller;

import com.test.bean.RequestResult;
import com.test.helper.ControllerHelper;
import com.test.service.UserServiceImpl;
import org.springframework.http.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class CustomerController {

    @Autowired
    public UserServiceImpl userService;

    //http://localhost:8080/api/user?idType=NEWNRIC&idNumber=900101-10-9943
    @RequestMapping(value = {"/user"}, method = RequestMethod.GET)
    public ResponseEntity<RequestResult> retrieve_user(@RequestParam(value = "idType", required = true) String idType,
                                                       @RequestParam(value = "idNumber", required = true) String idNumber) {


        return ControllerHelper.standardize(userService.getCustomer(idType, idNumber));
    }

    //http://localhost:8080/api/order?customerId=1001&startDate&endDate&statusCode&startRecord&recordLimit
    @RequestMapping(value = {"/order"}, method = RequestMethod.GET)
    public ResponseEntity<RequestResult> retrieve_order(@RequestParam(value = "customerId", required = true) String customerId,
                                                        @RequestParam(value = "startDate") String startDate,
                                                        @RequestParam(value = "endDate") String endDate,
                                                        @RequestParam(value = "statusCode") String statusCode,
                                                        @RequestParam(value = "startRecord") String startRecord,
                                                        @RequestParam(value = "recordLimit") String recordLimit) {


        return ControllerHelper.standardize(userService.getOrder(customerId, startDate, endDate, statusCode, startRecord, recordLimit));
    }

}
