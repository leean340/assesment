package com.test.bean;

import lombok.Data;

@Data
public class RequestResult {
    private Object meta;
    private Object content;
}
