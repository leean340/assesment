package com.test.bean;

import lombok.Data;

import java.util.Date;

@Data
public class CustomerBean {
    private Integer ctId;

    private String ctIdType;

    private String ctIdNumber;

    private String ctDisplayName;

    private String ctDob;

    private String ctEmail;

    private String ctContactId;

    private String ctContactType;

    private String ctContactValue;

    private String ctRolesCode;

    private String ctRolesDesc;

    private String ctNationality;

    private String ctRace;

    private String ctAddressId;

    private Boolean ctPrimaryaddress;

    private String ctLine1;

    private String ctLine2;

    private String ctLine3;

    private String ctPostalcode;

    private String ctCity;

    private String ctState;

    private String ctCountry;

    private String ctParam1;

    private String ctParam2;

    private String ctParam3;

    private String ctParam4;

    private String ctParam5;

    private Date ctCreatedDatetime;

    private Date ctUpdatedDatetime;

}