package com.test.bean;

import java.util.Date;

public class OrderBean {
    private Integer otId;

    private Integer otTrxRefId;

    private String otStatus;

    private String otStatusDesc;

    private String otRemarks;

    private String otParam1;

    private String otParam2;

    private String otParam3;

    private String otParam4;

    private String otParam5;

    private Date otCreatedDatetime;

    private Date otUpdatedDatetime;

    public Integer getOtId() {
        return otId;
    }

    public void setOtId(Integer otId) {
        this.otId = otId;
    }

    public Integer getOtTrxRefId() {
        return otTrxRefId;
    }

    public void setOtTrxRefId(Integer otTrxRefId) {
        this.otTrxRefId = otTrxRefId;
    }

    public String getOtStatus() {
        return otStatus;
    }

    public void setOtStatus(String otStatus) {
        this.otStatus = otStatus == null ? null : otStatus.trim();
    }

    public String getOtStatusDesc() {
        return otStatusDesc;
    }

    public void setOtStatusDesc(String otStatusDesc) {
        this.otStatusDesc = otStatusDesc == null ? null : otStatusDesc.trim();
    }

    public String getOtRemarks() {
        return otRemarks;
    }

    public void setOtRemarks(String otRemarks) {
        this.otRemarks = otRemarks == null ? null : otRemarks.trim();
    }

    public String getOtParam1() {
        return otParam1;
    }

    public void setOtParam1(String otParam1) {
        this.otParam1 = otParam1 == null ? null : otParam1.trim();
    }

    public String getOtParam2() {
        return otParam2;
    }

    public void setOtParam2(String otParam2) {
        this.otParam2 = otParam2 == null ? null : otParam2.trim();
    }

    public String getOtParam3() {
        return otParam3;
    }

    public void setOtParam3(String otParam3) {
        this.otParam3 = otParam3 == null ? null : otParam3.trim();
    }

    public String getOtParam4() {
        return otParam4;
    }

    public void setOtParam4(String otParam4) {
        this.otParam4 = otParam4 == null ? null : otParam4.trim();
    }

    public String getOtParam5() {
        return otParam5;
    }

    public void setOtParam5(String otParam5) {
        this.otParam5 = otParam5 == null ? null : otParam5.trim();
    }

    public Date getOtCreatedDatetime() {
        return otCreatedDatetime;
    }

    public void setOtCreatedDatetime(Date otCreatedDatetime) {
        this.otCreatedDatetime = otCreatedDatetime;
    }

    public Date getOtUpdatedDatetime() {
        return otUpdatedDatetime;
    }

    public void setOtUpdatedDatetime(Date otUpdatedDatetime) {
        this.otUpdatedDatetime = otUpdatedDatetime;
    }
}