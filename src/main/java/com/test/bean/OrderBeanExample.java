package com.test.bean;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class OrderBeanExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public OrderBeanExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andOtIdIsNull() {
            addCriterion("ot_id is null");
            return (Criteria) this;
        }

        public Criteria andOtIdIsNotNull() {
            addCriterion("ot_id is not null");
            return (Criteria) this;
        }

        public Criteria andOtIdEqualTo(Integer value) {
            addCriterion("ot_id =", value, "otId");
            return (Criteria) this;
        }

        public Criteria andOtIdNotEqualTo(Integer value) {
            addCriterion("ot_id <>", value, "otId");
            return (Criteria) this;
        }

        public Criteria andOtIdGreaterThan(Integer value) {
            addCriterion("ot_id >", value, "otId");
            return (Criteria) this;
        }

        public Criteria andOtIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("ot_id >=", value, "otId");
            return (Criteria) this;
        }

        public Criteria andOtIdLessThan(Integer value) {
            addCriterion("ot_id <", value, "otId");
            return (Criteria) this;
        }

        public Criteria andOtIdLessThanOrEqualTo(Integer value) {
            addCriterion("ot_id <=", value, "otId");
            return (Criteria) this;
        }

        public Criteria andOtIdIn(List<Integer> values) {
            addCriterion("ot_id in", values, "otId");
            return (Criteria) this;
        }

        public Criteria andOtIdNotIn(List<Integer> values) {
            addCriterion("ot_id not in", values, "otId");
            return (Criteria) this;
        }

        public Criteria andOtIdBetween(Integer value1, Integer value2) {
            addCriterion("ot_id between", value1, value2, "otId");
            return (Criteria) this;
        }

        public Criteria andOtIdNotBetween(Integer value1, Integer value2) {
            addCriterion("ot_id not between", value1, value2, "otId");
            return (Criteria) this;
        }

        public Criteria andOtTrxRefIdIsNull() {
            addCriterion("ot_trx_ref_id is null");
            return (Criteria) this;
        }

        public Criteria andOtTrxRefIdIsNotNull() {
            addCriterion("ot_trx_ref_id is not null");
            return (Criteria) this;
        }

        public Criteria andOtTrxRefIdEqualTo(Integer value) {
            addCriterion("ot_trx_ref_id =", value, "otTrxRefId");
            return (Criteria) this;
        }

        public Criteria andOtTrxRefIdNotEqualTo(Integer value) {
            addCriterion("ot_trx_ref_id <>", value, "otTrxRefId");
            return (Criteria) this;
        }

        public Criteria andOtTrxRefIdGreaterThan(Integer value) {
            addCriterion("ot_trx_ref_id >", value, "otTrxRefId");
            return (Criteria) this;
        }

        public Criteria andOtTrxRefIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("ot_trx_ref_id >=", value, "otTrxRefId");
            return (Criteria) this;
        }

        public Criteria andOtTrxRefIdLessThan(Integer value) {
            addCriterion("ot_trx_ref_id <", value, "otTrxRefId");
            return (Criteria) this;
        }

        public Criteria andOtTrxRefIdLessThanOrEqualTo(Integer value) {
            addCriterion("ot_trx_ref_id <=", value, "otTrxRefId");
            return (Criteria) this;
        }

        public Criteria andOtTrxRefIdIn(List<Integer> values) {
            addCriterion("ot_trx_ref_id in", values, "otTrxRefId");
            return (Criteria) this;
        }

        public Criteria andOtTrxRefIdNotIn(List<Integer> values) {
            addCriterion("ot_trx_ref_id not in", values, "otTrxRefId");
            return (Criteria) this;
        }

        public Criteria andOtTrxRefIdBetween(Integer value1, Integer value2) {
            addCriterion("ot_trx_ref_id between", value1, value2, "otTrxRefId");
            return (Criteria) this;
        }

        public Criteria andOtTrxRefIdNotBetween(Integer value1, Integer value2) {
            addCriterion("ot_trx_ref_id not between", value1, value2, "otTrxRefId");
            return (Criteria) this;
        }

        public Criteria andOtStatusIsNull() {
            addCriterion("ot_status is null");
            return (Criteria) this;
        }

        public Criteria andOtStatusIsNotNull() {
            addCriterion("ot_status is not null");
            return (Criteria) this;
        }

        public Criteria andOtStatusEqualTo(String value) {
            addCriterion("ot_status =", value, "otStatus");
            return (Criteria) this;
        }

        public Criteria andOtStatusNotEqualTo(String value) {
            addCriterion("ot_status <>", value, "otStatus");
            return (Criteria) this;
        }

        public Criteria andOtStatusGreaterThan(String value) {
            addCriterion("ot_status >", value, "otStatus");
            return (Criteria) this;
        }

        public Criteria andOtStatusGreaterThanOrEqualTo(String value) {
            addCriterion("ot_status >=", value, "otStatus");
            return (Criteria) this;
        }

        public Criteria andOtStatusLessThan(String value) {
            addCriterion("ot_status <", value, "otStatus");
            return (Criteria) this;
        }

        public Criteria andOtStatusLessThanOrEqualTo(String value) {
            addCriterion("ot_status <=", value, "otStatus");
            return (Criteria) this;
        }

        public Criteria andOtStatusLike(String value) {
            addCriterion("ot_status like", value, "otStatus");
            return (Criteria) this;
        }

        public Criteria andOtStatusNotLike(String value) {
            addCriterion("ot_status not like", value, "otStatus");
            return (Criteria) this;
        }

        public Criteria andOtStatusIn(List<String> values) {
            addCriterion("ot_status in", values, "otStatus");
            return (Criteria) this;
        }

        public Criteria andOtStatusNotIn(List<String> values) {
            addCriterion("ot_status not in", values, "otStatus");
            return (Criteria) this;
        }

        public Criteria andOtStatusBetween(String value1, String value2) {
            addCriterion("ot_status between", value1, value2, "otStatus");
            return (Criteria) this;
        }

        public Criteria andOtStatusNotBetween(String value1, String value2) {
            addCriterion("ot_status not between", value1, value2, "otStatus");
            return (Criteria) this;
        }

        public Criteria andOtStatusDescIsNull() {
            addCriterion("ot_status_desc is null");
            return (Criteria) this;
        }

        public Criteria andOtStatusDescIsNotNull() {
            addCriterion("ot_status_desc is not null");
            return (Criteria) this;
        }

        public Criteria andOtStatusDescEqualTo(String value) {
            addCriterion("ot_status_desc =", value, "otStatusDesc");
            return (Criteria) this;
        }

        public Criteria andOtStatusDescNotEqualTo(String value) {
            addCriterion("ot_status_desc <>", value, "otStatusDesc");
            return (Criteria) this;
        }

        public Criteria andOtStatusDescGreaterThan(String value) {
            addCriterion("ot_status_desc >", value, "otStatusDesc");
            return (Criteria) this;
        }

        public Criteria andOtStatusDescGreaterThanOrEqualTo(String value) {
            addCriterion("ot_status_desc >=", value, "otStatusDesc");
            return (Criteria) this;
        }

        public Criteria andOtStatusDescLessThan(String value) {
            addCriterion("ot_status_desc <", value, "otStatusDesc");
            return (Criteria) this;
        }

        public Criteria andOtStatusDescLessThanOrEqualTo(String value) {
            addCriterion("ot_status_desc <=", value, "otStatusDesc");
            return (Criteria) this;
        }

        public Criteria andOtStatusDescLike(String value) {
            addCriterion("ot_status_desc like", value, "otStatusDesc");
            return (Criteria) this;
        }

        public Criteria andOtStatusDescNotLike(String value) {
            addCriterion("ot_status_desc not like", value, "otStatusDesc");
            return (Criteria) this;
        }

        public Criteria andOtStatusDescIn(List<String> values) {
            addCriterion("ot_status_desc in", values, "otStatusDesc");
            return (Criteria) this;
        }

        public Criteria andOtStatusDescNotIn(List<String> values) {
            addCriterion("ot_status_desc not in", values, "otStatusDesc");
            return (Criteria) this;
        }

        public Criteria andOtStatusDescBetween(String value1, String value2) {
            addCriterion("ot_status_desc between", value1, value2, "otStatusDesc");
            return (Criteria) this;
        }

        public Criteria andOtStatusDescNotBetween(String value1, String value2) {
            addCriterion("ot_status_desc not between", value1, value2, "otStatusDesc");
            return (Criteria) this;
        }

        public Criteria andOtRemarksIsNull() {
            addCriterion("ot_remarks is null");
            return (Criteria) this;
        }

        public Criteria andOtRemarksIsNotNull() {
            addCriterion("ot_remarks is not null");
            return (Criteria) this;
        }

        public Criteria andOtRemarksEqualTo(String value) {
            addCriterion("ot_remarks =", value, "otRemarks");
            return (Criteria) this;
        }

        public Criteria andOtRemarksNotEqualTo(String value) {
            addCriterion("ot_remarks <>", value, "otRemarks");
            return (Criteria) this;
        }

        public Criteria andOtRemarksGreaterThan(String value) {
            addCriterion("ot_remarks >", value, "otRemarks");
            return (Criteria) this;
        }

        public Criteria andOtRemarksGreaterThanOrEqualTo(String value) {
            addCriterion("ot_remarks >=", value, "otRemarks");
            return (Criteria) this;
        }

        public Criteria andOtRemarksLessThan(String value) {
            addCriterion("ot_remarks <", value, "otRemarks");
            return (Criteria) this;
        }

        public Criteria andOtRemarksLessThanOrEqualTo(String value) {
            addCriterion("ot_remarks <=", value, "otRemarks");
            return (Criteria) this;
        }

        public Criteria andOtRemarksLike(String value) {
            addCriterion("ot_remarks like", value, "otRemarks");
            return (Criteria) this;
        }

        public Criteria andOtRemarksNotLike(String value) {
            addCriterion("ot_remarks not like", value, "otRemarks");
            return (Criteria) this;
        }

        public Criteria andOtRemarksIn(List<String> values) {
            addCriterion("ot_remarks in", values, "otRemarks");
            return (Criteria) this;
        }

        public Criteria andOtRemarksNotIn(List<String> values) {
            addCriterion("ot_remarks not in", values, "otRemarks");
            return (Criteria) this;
        }

        public Criteria andOtRemarksBetween(String value1, String value2) {
            addCriterion("ot_remarks between", value1, value2, "otRemarks");
            return (Criteria) this;
        }

        public Criteria andOtRemarksNotBetween(String value1, String value2) {
            addCriterion("ot_remarks not between", value1, value2, "otRemarks");
            return (Criteria) this;
        }

        public Criteria andOtParam1IsNull() {
            addCriterion("ot_param1 is null");
            return (Criteria) this;
        }

        public Criteria andOtParam1IsNotNull() {
            addCriterion("ot_param1 is not null");
            return (Criteria) this;
        }

        public Criteria andOtParam1EqualTo(String value) {
            addCriterion("ot_param1 =", value, "otParam1");
            return (Criteria) this;
        }

        public Criteria andOtParam1NotEqualTo(String value) {
            addCriterion("ot_param1 <>", value, "otParam1");
            return (Criteria) this;
        }

        public Criteria andOtParam1GreaterThan(String value) {
            addCriterion("ot_param1 >", value, "otParam1");
            return (Criteria) this;
        }

        public Criteria andOtParam1GreaterThanOrEqualTo(String value) {
            addCriterion("ot_param1 >=", value, "otParam1");
            return (Criteria) this;
        }

        public Criteria andOtParam1LessThan(String value) {
            addCriterion("ot_param1 <", value, "otParam1");
            return (Criteria) this;
        }

        public Criteria andOtParam1LessThanOrEqualTo(String value) {
            addCriterion("ot_param1 <=", value, "otParam1");
            return (Criteria) this;
        }

        public Criteria andOtParam1Like(String value) {
            addCriterion("ot_param1 like", value, "otParam1");
            return (Criteria) this;
        }

        public Criteria andOtParam1NotLike(String value) {
            addCriterion("ot_param1 not like", value, "otParam1");
            return (Criteria) this;
        }

        public Criteria andOtParam1In(List<String> values) {
            addCriterion("ot_param1 in", values, "otParam1");
            return (Criteria) this;
        }

        public Criteria andOtParam1NotIn(List<String> values) {
            addCriterion("ot_param1 not in", values, "otParam1");
            return (Criteria) this;
        }

        public Criteria andOtParam1Between(String value1, String value2) {
            addCriterion("ot_param1 between", value1, value2, "otParam1");
            return (Criteria) this;
        }

        public Criteria andOtParam1NotBetween(String value1, String value2) {
            addCriterion("ot_param1 not between", value1, value2, "otParam1");
            return (Criteria) this;
        }

        public Criteria andOtParam2IsNull() {
            addCriterion("ot_param2 is null");
            return (Criteria) this;
        }

        public Criteria andOtParam2IsNotNull() {
            addCriterion("ot_param2 is not null");
            return (Criteria) this;
        }

        public Criteria andOtParam2EqualTo(String value) {
            addCriterion("ot_param2 =", value, "otParam2");
            return (Criteria) this;
        }

        public Criteria andOtParam2NotEqualTo(String value) {
            addCriterion("ot_param2 <>", value, "otParam2");
            return (Criteria) this;
        }

        public Criteria andOtParam2GreaterThan(String value) {
            addCriterion("ot_param2 >", value, "otParam2");
            return (Criteria) this;
        }

        public Criteria andOtParam2GreaterThanOrEqualTo(String value) {
            addCriterion("ot_param2 >=", value, "otParam2");
            return (Criteria) this;
        }

        public Criteria andOtParam2LessThan(String value) {
            addCriterion("ot_param2 <", value, "otParam2");
            return (Criteria) this;
        }

        public Criteria andOtParam2LessThanOrEqualTo(String value) {
            addCriterion("ot_param2 <=", value, "otParam2");
            return (Criteria) this;
        }

        public Criteria andOtParam2Like(String value) {
            addCriterion("ot_param2 like", value, "otParam2");
            return (Criteria) this;
        }

        public Criteria andOtParam2NotLike(String value) {
            addCriterion("ot_param2 not like", value, "otParam2");
            return (Criteria) this;
        }

        public Criteria andOtParam2In(List<String> values) {
            addCriterion("ot_param2 in", values, "otParam2");
            return (Criteria) this;
        }

        public Criteria andOtParam2NotIn(List<String> values) {
            addCriterion("ot_param2 not in", values, "otParam2");
            return (Criteria) this;
        }

        public Criteria andOtParam2Between(String value1, String value2) {
            addCriterion("ot_param2 between", value1, value2, "otParam2");
            return (Criteria) this;
        }

        public Criteria andOtParam2NotBetween(String value1, String value2) {
            addCriterion("ot_param2 not between", value1, value2, "otParam2");
            return (Criteria) this;
        }

        public Criteria andOtParam3IsNull() {
            addCriterion("ot_param3 is null");
            return (Criteria) this;
        }

        public Criteria andOtParam3IsNotNull() {
            addCriterion("ot_param3 is not null");
            return (Criteria) this;
        }

        public Criteria andOtParam3EqualTo(String value) {
            addCriterion("ot_param3 =", value, "otParam3");
            return (Criteria) this;
        }

        public Criteria andOtParam3NotEqualTo(String value) {
            addCriterion("ot_param3 <>", value, "otParam3");
            return (Criteria) this;
        }

        public Criteria andOtParam3GreaterThan(String value) {
            addCriterion("ot_param3 >", value, "otParam3");
            return (Criteria) this;
        }

        public Criteria andOtParam3GreaterThanOrEqualTo(String value) {
            addCriterion("ot_param3 >=", value, "otParam3");
            return (Criteria) this;
        }

        public Criteria andOtParam3LessThan(String value) {
            addCriterion("ot_param3 <", value, "otParam3");
            return (Criteria) this;
        }

        public Criteria andOtParam3LessThanOrEqualTo(String value) {
            addCriterion("ot_param3 <=", value, "otParam3");
            return (Criteria) this;
        }

        public Criteria andOtParam3Like(String value) {
            addCriterion("ot_param3 like", value, "otParam3");
            return (Criteria) this;
        }

        public Criteria andOtParam3NotLike(String value) {
            addCriterion("ot_param3 not like", value, "otParam3");
            return (Criteria) this;
        }

        public Criteria andOtParam3In(List<String> values) {
            addCriterion("ot_param3 in", values, "otParam3");
            return (Criteria) this;
        }

        public Criteria andOtParam3NotIn(List<String> values) {
            addCriterion("ot_param3 not in", values, "otParam3");
            return (Criteria) this;
        }

        public Criteria andOtParam3Between(String value1, String value2) {
            addCriterion("ot_param3 between", value1, value2, "otParam3");
            return (Criteria) this;
        }

        public Criteria andOtParam3NotBetween(String value1, String value2) {
            addCriterion("ot_param3 not between", value1, value2, "otParam3");
            return (Criteria) this;
        }

        public Criteria andOtParam4IsNull() {
            addCriterion("ot_param4 is null");
            return (Criteria) this;
        }

        public Criteria andOtParam4IsNotNull() {
            addCriterion("ot_param4 is not null");
            return (Criteria) this;
        }

        public Criteria andOtParam4EqualTo(String value) {
            addCriterion("ot_param4 =", value, "otParam4");
            return (Criteria) this;
        }

        public Criteria andOtParam4NotEqualTo(String value) {
            addCriterion("ot_param4 <>", value, "otParam4");
            return (Criteria) this;
        }

        public Criteria andOtParam4GreaterThan(String value) {
            addCriterion("ot_param4 >", value, "otParam4");
            return (Criteria) this;
        }

        public Criteria andOtParam4GreaterThanOrEqualTo(String value) {
            addCriterion("ot_param4 >=", value, "otParam4");
            return (Criteria) this;
        }

        public Criteria andOtParam4LessThan(String value) {
            addCriterion("ot_param4 <", value, "otParam4");
            return (Criteria) this;
        }

        public Criteria andOtParam4LessThanOrEqualTo(String value) {
            addCriterion("ot_param4 <=", value, "otParam4");
            return (Criteria) this;
        }

        public Criteria andOtParam4Like(String value) {
            addCriterion("ot_param4 like", value, "otParam4");
            return (Criteria) this;
        }

        public Criteria andOtParam4NotLike(String value) {
            addCriterion("ot_param4 not like", value, "otParam4");
            return (Criteria) this;
        }

        public Criteria andOtParam4In(List<String> values) {
            addCriterion("ot_param4 in", values, "otParam4");
            return (Criteria) this;
        }

        public Criteria andOtParam4NotIn(List<String> values) {
            addCriterion("ot_param4 not in", values, "otParam4");
            return (Criteria) this;
        }

        public Criteria andOtParam4Between(String value1, String value2) {
            addCriterion("ot_param4 between", value1, value2, "otParam4");
            return (Criteria) this;
        }

        public Criteria andOtParam4NotBetween(String value1, String value2) {
            addCriterion("ot_param4 not between", value1, value2, "otParam4");
            return (Criteria) this;
        }

        public Criteria andOtParam5IsNull() {
            addCriterion("ot_param5 is null");
            return (Criteria) this;
        }

        public Criteria andOtParam5IsNotNull() {
            addCriterion("ot_param5 is not null");
            return (Criteria) this;
        }

        public Criteria andOtParam5EqualTo(String value) {
            addCriterion("ot_param5 =", value, "otParam5");
            return (Criteria) this;
        }

        public Criteria andOtParam5NotEqualTo(String value) {
            addCriterion("ot_param5 <>", value, "otParam5");
            return (Criteria) this;
        }

        public Criteria andOtParam5GreaterThan(String value) {
            addCriterion("ot_param5 >", value, "otParam5");
            return (Criteria) this;
        }

        public Criteria andOtParam5GreaterThanOrEqualTo(String value) {
            addCriterion("ot_param5 >=", value, "otParam5");
            return (Criteria) this;
        }

        public Criteria andOtParam5LessThan(String value) {
            addCriterion("ot_param5 <", value, "otParam5");
            return (Criteria) this;
        }

        public Criteria andOtParam5LessThanOrEqualTo(String value) {
            addCriterion("ot_param5 <=", value, "otParam5");
            return (Criteria) this;
        }

        public Criteria andOtParam5Like(String value) {
            addCriterion("ot_param5 like", value, "otParam5");
            return (Criteria) this;
        }

        public Criteria andOtParam5NotLike(String value) {
            addCriterion("ot_param5 not like", value, "otParam5");
            return (Criteria) this;
        }

        public Criteria andOtParam5In(List<String> values) {
            addCriterion("ot_param5 in", values, "otParam5");
            return (Criteria) this;
        }

        public Criteria andOtParam5NotIn(List<String> values) {
            addCriterion("ot_param5 not in", values, "otParam5");
            return (Criteria) this;
        }

        public Criteria andOtParam5Between(String value1, String value2) {
            addCriterion("ot_param5 between", value1, value2, "otParam5");
            return (Criteria) this;
        }

        public Criteria andOtParam5NotBetween(String value1, String value2) {
            addCriterion("ot_param5 not between", value1, value2, "otParam5");
            return (Criteria) this;
        }

        public Criteria andOtCreatedDatetimeIsNull() {
            addCriterion("ot_created_datetime is null");
            return (Criteria) this;
        }

        public Criteria andOtCreatedDatetimeIsNotNull() {
            addCriterion("ot_created_datetime is not null");
            return (Criteria) this;
        }

        public Criteria andOtCreatedDatetimeEqualTo(Date value) {
            addCriterion("ot_created_datetime =", value, "otCreatedDatetime");
            return (Criteria) this;
        }

        public Criteria andOtCreatedDatetimeNotEqualTo(Date value) {
            addCriterion("ot_created_datetime <>", value, "otCreatedDatetime");
            return (Criteria) this;
        }

        public Criteria andOtCreatedDatetimeGreaterThan(Date value) {
            addCriterion("ot_created_datetime >", value, "otCreatedDatetime");
            return (Criteria) this;
        }

        public Criteria andOtCreatedDatetimeGreaterThanOrEqualTo(Date value) {
            addCriterion("ot_created_datetime >=", value, "otCreatedDatetime");
            return (Criteria) this;
        }

        public Criteria andOtCreatedDatetimeLessThan(Date value) {
            addCriterion("ot_created_datetime <", value, "otCreatedDatetime");
            return (Criteria) this;
        }

        public Criteria andOtCreatedDatetimeLessThanOrEqualTo(Date value) {
            addCriterion("ot_created_datetime <=", value, "otCreatedDatetime");
            return (Criteria) this;
        }

        public Criteria andOtCreatedDatetimeIn(List<Date> values) {
            addCriterion("ot_created_datetime in", values, "otCreatedDatetime");
            return (Criteria) this;
        }

        public Criteria andOtCreatedDatetimeNotIn(List<Date> values) {
            addCriterion("ot_created_datetime not in", values, "otCreatedDatetime");
            return (Criteria) this;
        }

        public Criteria andOtCreatedDatetimeBetween(Date value1, Date value2) {
            addCriterion("ot_created_datetime between", value1, value2, "otCreatedDatetime");
            return (Criteria) this;
        }

        public Criteria andOtCreatedDatetimeNotBetween(Date value1, Date value2) {
            addCriterion("ot_created_datetime not between", value1, value2, "otCreatedDatetime");
            return (Criteria) this;
        }

        public Criteria andOtUpdatedDatetimeIsNull() {
            addCriterion("ot_updated_datetime is null");
            return (Criteria) this;
        }

        public Criteria andOtUpdatedDatetimeIsNotNull() {
            addCriterion("ot_updated_datetime is not null");
            return (Criteria) this;
        }

        public Criteria andOtUpdatedDatetimeEqualTo(Date value) {
            addCriterion("ot_updated_datetime =", value, "otUpdatedDatetime");
            return (Criteria) this;
        }

        public Criteria andOtUpdatedDatetimeNotEqualTo(Date value) {
            addCriterion("ot_updated_datetime <>", value, "otUpdatedDatetime");
            return (Criteria) this;
        }

        public Criteria andOtUpdatedDatetimeGreaterThan(Date value) {
            addCriterion("ot_updated_datetime >", value, "otUpdatedDatetime");
            return (Criteria) this;
        }

        public Criteria andOtUpdatedDatetimeGreaterThanOrEqualTo(Date value) {
            addCriterion("ot_updated_datetime >=", value, "otUpdatedDatetime");
            return (Criteria) this;
        }

        public Criteria andOtUpdatedDatetimeLessThan(Date value) {
            addCriterion("ot_updated_datetime <", value, "otUpdatedDatetime");
            return (Criteria) this;
        }

        public Criteria andOtUpdatedDatetimeLessThanOrEqualTo(Date value) {
            addCriterion("ot_updated_datetime <=", value, "otUpdatedDatetime");
            return (Criteria) this;
        }

        public Criteria andOtUpdatedDatetimeIn(List<Date> values) {
            addCriterion("ot_updated_datetime in", values, "otUpdatedDatetime");
            return (Criteria) this;
        }

        public Criteria andOtUpdatedDatetimeNotIn(List<Date> values) {
            addCriterion("ot_updated_datetime not in", values, "otUpdatedDatetime");
            return (Criteria) this;
        }

        public Criteria andOtUpdatedDatetimeBetween(Date value1, Date value2) {
            addCriterion("ot_updated_datetime between", value1, value2, "otUpdatedDatetime");
            return (Criteria) this;
        }

        public Criteria andOtUpdatedDatetimeNotBetween(Date value1, Date value2) {
            addCriterion("ot_updated_datetime not between", value1, value2, "otUpdatedDatetime");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}