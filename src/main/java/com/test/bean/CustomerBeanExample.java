package com.test.bean;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CustomerBeanExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public CustomerBeanExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andCtIdIsNull() {
            addCriterion("ct_id is null");
            return (Criteria) this;
        }

        public Criteria andCtIdIsNotNull() {
            addCriterion("ct_id is not null");
            return (Criteria) this;
        }

        public Criteria andCtIdEqualTo(Integer value) {
            addCriterion("ct_id =", value, "ctId");
            return (Criteria) this;
        }

        public Criteria andCtIdNotEqualTo(Integer value) {
            addCriterion("ct_id <>", value, "ctId");
            return (Criteria) this;
        }

        public Criteria andCtIdGreaterThan(Integer value) {
            addCriterion("ct_id >", value, "ctId");
            return (Criteria) this;
        }

        public Criteria andCtIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("ct_id >=", value, "ctId");
            return (Criteria) this;
        }

        public Criteria andCtIdLessThan(Integer value) {
            addCriterion("ct_id <", value, "ctId");
            return (Criteria) this;
        }

        public Criteria andCtIdLessThanOrEqualTo(Integer value) {
            addCriterion("ct_id <=", value, "ctId");
            return (Criteria) this;
        }

        public Criteria andCtIdIn(List<Integer> values) {
            addCriterion("ct_id in", values, "ctId");
            return (Criteria) this;
        }

        public Criteria andCtIdNotIn(List<Integer> values) {
            addCriterion("ct_id not in", values, "ctId");
            return (Criteria) this;
        }

        public Criteria andCtIdBetween(Integer value1, Integer value2) {
            addCriterion("ct_id between", value1, value2, "ctId");
            return (Criteria) this;
        }

        public Criteria andCtIdNotBetween(Integer value1, Integer value2) {
            addCriterion("ct_id not between", value1, value2, "ctId");
            return (Criteria) this;
        }

        public Criteria andCtIdTypeIsNull() {
            addCriterion("ct_id_type is null");
            return (Criteria) this;
        }

        public Criteria andCtIdTypeIsNotNull() {
            addCriterion("ct_id_type is not null");
            return (Criteria) this;
        }

        public Criteria andCtIdTypeEqualTo(String value) {
            addCriterion("ct_id_type =", value, "ctIdType");
            return (Criteria) this;
        }

        public Criteria andCtIdTypeNotEqualTo(String value) {
            addCriterion("ct_id_type <>", value, "ctIdType");
            return (Criteria) this;
        }

        public Criteria andCtIdTypeGreaterThan(String value) {
            addCriterion("ct_id_type >", value, "ctIdType");
            return (Criteria) this;
        }

        public Criteria andCtIdTypeGreaterThanOrEqualTo(String value) {
            addCriterion("ct_id_type >=", value, "ctIdType");
            return (Criteria) this;
        }

        public Criteria andCtIdTypeLessThan(String value) {
            addCriterion("ct_id_type <", value, "ctIdType");
            return (Criteria) this;
        }

        public Criteria andCtIdTypeLessThanOrEqualTo(String value) {
            addCriterion("ct_id_type <=", value, "ctIdType");
            return (Criteria) this;
        }

        public Criteria andCtIdTypeLike(String value) {
            addCriterion("ct_id_type like", value, "ctIdType");
            return (Criteria) this;
        }

        public Criteria andCtIdTypeNotLike(String value) {
            addCriterion("ct_id_type not like", value, "ctIdType");
            return (Criteria) this;
        }

        public Criteria andCtIdTypeIn(List<String> values) {
            addCriterion("ct_id_type in", values, "ctIdType");
            return (Criteria) this;
        }

        public Criteria andCtIdTypeNotIn(List<String> values) {
            addCriterion("ct_id_type not in", values, "ctIdType");
            return (Criteria) this;
        }

        public Criteria andCtIdTypeBetween(String value1, String value2) {
            addCriterion("ct_id_type between", value1, value2, "ctIdType");
            return (Criteria) this;
        }

        public Criteria andCtIdTypeNotBetween(String value1, String value2) {
            addCriterion("ct_id_type not between", value1, value2, "ctIdType");
            return (Criteria) this;
        }

        public Criteria andCtIdNumberIsNull() {
            addCriterion("ct_id_number is null");
            return (Criteria) this;
        }

        public Criteria andCtIdNumberIsNotNull() {
            addCriterion("ct_id_number is not null");
            return (Criteria) this;
        }

        public Criteria andCtIdNumberEqualTo(String value) {
            addCriterion("ct_id_number =", value, "ctIdNumber");
            return (Criteria) this;
        }

        public Criteria andCtIdNumberNotEqualTo(String value) {
            addCriterion("ct_id_number <>", value, "ctIdNumber");
            return (Criteria) this;
        }

        public Criteria andCtIdNumberGreaterThan(String value) {
            addCriterion("ct_id_number >", value, "ctIdNumber");
            return (Criteria) this;
        }

        public Criteria andCtIdNumberGreaterThanOrEqualTo(String value) {
            addCriterion("ct_id_number >=", value, "ctIdNumber");
            return (Criteria) this;
        }

        public Criteria andCtIdNumberLessThan(String value) {
            addCriterion("ct_id_number <", value, "ctIdNumber");
            return (Criteria) this;
        }

        public Criteria andCtIdNumberLessThanOrEqualTo(String value) {
            addCriterion("ct_id_number <=", value, "ctIdNumber");
            return (Criteria) this;
        }

        public Criteria andCtIdNumberLike(String value) {
            addCriterion("ct_id_number like", value, "ctIdNumber");
            return (Criteria) this;
        }

        public Criteria andCtIdNumberNotLike(String value) {
            addCriterion("ct_id_number not like", value, "ctIdNumber");
            return (Criteria) this;
        }

        public Criteria andCtIdNumberIn(List<String> values) {
            addCriterion("ct_id_number in", values, "ctIdNumber");
            return (Criteria) this;
        }

        public Criteria andCtIdNumberNotIn(List<String> values) {
            addCriterion("ct_id_number not in", values, "ctIdNumber");
            return (Criteria) this;
        }

        public Criteria andCtIdNumberBetween(String value1, String value2) {
            addCriterion("ct_id_number between", value1, value2, "ctIdNumber");
            return (Criteria) this;
        }

        public Criteria andCtIdNumberNotBetween(String value1, String value2) {
            addCriterion("ct_id_number not between", value1, value2, "ctIdNumber");
            return (Criteria) this;
        }

        public Criteria andCtDisplayNameIsNull() {
            addCriterion("ct_display_name is null");
            return (Criteria) this;
        }

        public Criteria andCtDisplayNameIsNotNull() {
            addCriterion("ct_display_name is not null");
            return (Criteria) this;
        }

        public Criteria andCtDisplayNameEqualTo(String value) {
            addCriterion("ct_display_name =", value, "ctDisplayName");
            return (Criteria) this;
        }

        public Criteria andCtDisplayNameNotEqualTo(String value) {
            addCriterion("ct_display_name <>", value, "ctDisplayName");
            return (Criteria) this;
        }

        public Criteria andCtDisplayNameGreaterThan(String value) {
            addCriterion("ct_display_name >", value, "ctDisplayName");
            return (Criteria) this;
        }

        public Criteria andCtDisplayNameGreaterThanOrEqualTo(String value) {
            addCriterion("ct_display_name >=", value, "ctDisplayName");
            return (Criteria) this;
        }

        public Criteria andCtDisplayNameLessThan(String value) {
            addCriterion("ct_display_name <", value, "ctDisplayName");
            return (Criteria) this;
        }

        public Criteria andCtDisplayNameLessThanOrEqualTo(String value) {
            addCriterion("ct_display_name <=", value, "ctDisplayName");
            return (Criteria) this;
        }

        public Criteria andCtDisplayNameLike(String value) {
            addCriterion("ct_display_name like", value, "ctDisplayName");
            return (Criteria) this;
        }

        public Criteria andCtDisplayNameNotLike(String value) {
            addCriterion("ct_display_name not like", value, "ctDisplayName");
            return (Criteria) this;
        }

        public Criteria andCtDisplayNameIn(List<String> values) {
            addCriterion("ct_display_name in", values, "ctDisplayName");
            return (Criteria) this;
        }

        public Criteria andCtDisplayNameNotIn(List<String> values) {
            addCriterion("ct_display_name not in", values, "ctDisplayName");
            return (Criteria) this;
        }

        public Criteria andCtDisplayNameBetween(String value1, String value2) {
            addCriterion("ct_display_name between", value1, value2, "ctDisplayName");
            return (Criteria) this;
        }

        public Criteria andCtDisplayNameNotBetween(String value1, String value2) {
            addCriterion("ct_display_name not between", value1, value2, "ctDisplayName");
            return (Criteria) this;
        }

        public Criteria andCtDobIsNull() {
            addCriterion("ct_dob is null");
            return (Criteria) this;
        }

        public Criteria andCtDobIsNotNull() {
            addCriterion("ct_dob is not null");
            return (Criteria) this;
        }

        public Criteria andCtDobEqualTo(String value) {
            addCriterion("ct_dob =", value, "ctDob");
            return (Criteria) this;
        }

        public Criteria andCtDobNotEqualTo(String value) {
            addCriterion("ct_dob <>", value, "ctDob");
            return (Criteria) this;
        }

        public Criteria andCtDobGreaterThan(String value) {
            addCriterion("ct_dob >", value, "ctDob");
            return (Criteria) this;
        }

        public Criteria andCtDobGreaterThanOrEqualTo(String value) {
            addCriterion("ct_dob >=", value, "ctDob");
            return (Criteria) this;
        }

        public Criteria andCtDobLessThan(String value) {
            addCriterion("ct_dob <", value, "ctDob");
            return (Criteria) this;
        }

        public Criteria andCtDobLessThanOrEqualTo(String value) {
            addCriterion("ct_dob <=", value, "ctDob");
            return (Criteria) this;
        }

        public Criteria andCtDobLike(String value) {
            addCriterion("ct_dob like", value, "ctDob");
            return (Criteria) this;
        }

        public Criteria andCtDobNotLike(String value) {
            addCriterion("ct_dob not like", value, "ctDob");
            return (Criteria) this;
        }

        public Criteria andCtDobIn(List<String> values) {
            addCriterion("ct_dob in", values, "ctDob");
            return (Criteria) this;
        }

        public Criteria andCtDobNotIn(List<String> values) {
            addCriterion("ct_dob not in", values, "ctDob");
            return (Criteria) this;
        }

        public Criteria andCtDobBetween(String value1, String value2) {
            addCriterion("ct_dob between", value1, value2, "ctDob");
            return (Criteria) this;
        }

        public Criteria andCtDobNotBetween(String value1, String value2) {
            addCriterion("ct_dob not between", value1, value2, "ctDob");
            return (Criteria) this;
        }

        public Criteria andCtEmailIsNull() {
            addCriterion("ct_email is null");
            return (Criteria) this;
        }

        public Criteria andCtEmailIsNotNull() {
            addCriterion("ct_email is not null");
            return (Criteria) this;
        }

        public Criteria andCtEmailEqualTo(String value) {
            addCriterion("ct_email =", value, "ctEmail");
            return (Criteria) this;
        }

        public Criteria andCtEmailNotEqualTo(String value) {
            addCriterion("ct_email <>", value, "ctEmail");
            return (Criteria) this;
        }

        public Criteria andCtEmailGreaterThan(String value) {
            addCriterion("ct_email >", value, "ctEmail");
            return (Criteria) this;
        }

        public Criteria andCtEmailGreaterThanOrEqualTo(String value) {
            addCriterion("ct_email >=", value, "ctEmail");
            return (Criteria) this;
        }

        public Criteria andCtEmailLessThan(String value) {
            addCriterion("ct_email <", value, "ctEmail");
            return (Criteria) this;
        }

        public Criteria andCtEmailLessThanOrEqualTo(String value) {
            addCriterion("ct_email <=", value, "ctEmail");
            return (Criteria) this;
        }

        public Criteria andCtEmailLike(String value) {
            addCriterion("ct_email like", value, "ctEmail");
            return (Criteria) this;
        }

        public Criteria andCtEmailNotLike(String value) {
            addCriterion("ct_email not like", value, "ctEmail");
            return (Criteria) this;
        }

        public Criteria andCtEmailIn(List<String> values) {
            addCriterion("ct_email in", values, "ctEmail");
            return (Criteria) this;
        }

        public Criteria andCtEmailNotIn(List<String> values) {
            addCriterion("ct_email not in", values, "ctEmail");
            return (Criteria) this;
        }

        public Criteria andCtEmailBetween(String value1, String value2) {
            addCriterion("ct_email between", value1, value2, "ctEmail");
            return (Criteria) this;
        }

        public Criteria andCtEmailNotBetween(String value1, String value2) {
            addCriterion("ct_email not between", value1, value2, "ctEmail");
            return (Criteria) this;
        }

        public Criteria andCtContactIdIsNull() {
            addCriterion("ct_contact_id is null");
            return (Criteria) this;
        }

        public Criteria andCtContactIdIsNotNull() {
            addCriterion("ct_contact_id is not null");
            return (Criteria) this;
        }

        public Criteria andCtContactIdEqualTo(String value) {
            addCriterion("ct_contact_id =", value, "ctContactId");
            return (Criteria) this;
        }

        public Criteria andCtContactIdNotEqualTo(String value) {
            addCriterion("ct_contact_id <>", value, "ctContactId");
            return (Criteria) this;
        }

        public Criteria andCtContactIdGreaterThan(String value) {
            addCriterion("ct_contact_id >", value, "ctContactId");
            return (Criteria) this;
        }

        public Criteria andCtContactIdGreaterThanOrEqualTo(String value) {
            addCriterion("ct_contact_id >=", value, "ctContactId");
            return (Criteria) this;
        }

        public Criteria andCtContactIdLessThan(String value) {
            addCriterion("ct_contact_id <", value, "ctContactId");
            return (Criteria) this;
        }

        public Criteria andCtContactIdLessThanOrEqualTo(String value) {
            addCriterion("ct_contact_id <=", value, "ctContactId");
            return (Criteria) this;
        }

        public Criteria andCtContactIdLike(String value) {
            addCriterion("ct_contact_id like", value, "ctContactId");
            return (Criteria) this;
        }

        public Criteria andCtContactIdNotLike(String value) {
            addCriterion("ct_contact_id not like", value, "ctContactId");
            return (Criteria) this;
        }

        public Criteria andCtContactIdIn(List<String> values) {
            addCriterion("ct_contact_id in", values, "ctContactId");
            return (Criteria) this;
        }

        public Criteria andCtContactIdNotIn(List<String> values) {
            addCriterion("ct_contact_id not in", values, "ctContactId");
            return (Criteria) this;
        }

        public Criteria andCtContactIdBetween(String value1, String value2) {
            addCriterion("ct_contact_id between", value1, value2, "ctContactId");
            return (Criteria) this;
        }

        public Criteria andCtContactIdNotBetween(String value1, String value2) {
            addCriterion("ct_contact_id not between", value1, value2, "ctContactId");
            return (Criteria) this;
        }

        public Criteria andCtContactTypeIsNull() {
            addCriterion("ct_contact_type is null");
            return (Criteria) this;
        }

        public Criteria andCtContactTypeIsNotNull() {
            addCriterion("ct_contact_type is not null");
            return (Criteria) this;
        }

        public Criteria andCtContactTypeEqualTo(String value) {
            addCriterion("ct_contact_type =", value, "ctContactType");
            return (Criteria) this;
        }

        public Criteria andCtContactTypeNotEqualTo(String value) {
            addCriterion("ct_contact_type <>", value, "ctContactType");
            return (Criteria) this;
        }

        public Criteria andCtContactTypeGreaterThan(String value) {
            addCriterion("ct_contact_type >", value, "ctContactType");
            return (Criteria) this;
        }

        public Criteria andCtContactTypeGreaterThanOrEqualTo(String value) {
            addCriterion("ct_contact_type >=", value, "ctContactType");
            return (Criteria) this;
        }

        public Criteria andCtContactTypeLessThan(String value) {
            addCriterion("ct_contact_type <", value, "ctContactType");
            return (Criteria) this;
        }

        public Criteria andCtContactTypeLessThanOrEqualTo(String value) {
            addCriterion("ct_contact_type <=", value, "ctContactType");
            return (Criteria) this;
        }

        public Criteria andCtContactTypeLike(String value) {
            addCriterion("ct_contact_type like", value, "ctContactType");
            return (Criteria) this;
        }

        public Criteria andCtContactTypeNotLike(String value) {
            addCriterion("ct_contact_type not like", value, "ctContactType");
            return (Criteria) this;
        }

        public Criteria andCtContactTypeIn(List<String> values) {
            addCriterion("ct_contact_type in", values, "ctContactType");
            return (Criteria) this;
        }

        public Criteria andCtContactTypeNotIn(List<String> values) {
            addCriterion("ct_contact_type not in", values, "ctContactType");
            return (Criteria) this;
        }

        public Criteria andCtContactTypeBetween(String value1, String value2) {
            addCriterion("ct_contact_type between", value1, value2, "ctContactType");
            return (Criteria) this;
        }

        public Criteria andCtContactTypeNotBetween(String value1, String value2) {
            addCriterion("ct_contact_type not between", value1, value2, "ctContactType");
            return (Criteria) this;
        }

        public Criteria andCtContactValueIsNull() {
            addCriterion("ct_contact_value is null");
            return (Criteria) this;
        }

        public Criteria andCtContactValueIsNotNull() {
            addCriterion("ct_contact_value is not null");
            return (Criteria) this;
        }

        public Criteria andCtContactValueEqualTo(String value) {
            addCriterion("ct_contact_value =", value, "ctContactValue");
            return (Criteria) this;
        }

        public Criteria andCtContactValueNotEqualTo(String value) {
            addCriterion("ct_contact_value <>", value, "ctContactValue");
            return (Criteria) this;
        }

        public Criteria andCtContactValueGreaterThan(String value) {
            addCriterion("ct_contact_value >", value, "ctContactValue");
            return (Criteria) this;
        }

        public Criteria andCtContactValueGreaterThanOrEqualTo(String value) {
            addCriterion("ct_contact_value >=", value, "ctContactValue");
            return (Criteria) this;
        }

        public Criteria andCtContactValueLessThan(String value) {
            addCriterion("ct_contact_value <", value, "ctContactValue");
            return (Criteria) this;
        }

        public Criteria andCtContactValueLessThanOrEqualTo(String value) {
            addCriterion("ct_contact_value <=", value, "ctContactValue");
            return (Criteria) this;
        }

        public Criteria andCtContactValueLike(String value) {
            addCriterion("ct_contact_value like", value, "ctContactValue");
            return (Criteria) this;
        }

        public Criteria andCtContactValueNotLike(String value) {
            addCriterion("ct_contact_value not like", value, "ctContactValue");
            return (Criteria) this;
        }

        public Criteria andCtContactValueIn(List<String> values) {
            addCriterion("ct_contact_value in", values, "ctContactValue");
            return (Criteria) this;
        }

        public Criteria andCtContactValueNotIn(List<String> values) {
            addCriterion("ct_contact_value not in", values, "ctContactValue");
            return (Criteria) this;
        }

        public Criteria andCtContactValueBetween(String value1, String value2) {
            addCriterion("ct_contact_value between", value1, value2, "ctContactValue");
            return (Criteria) this;
        }

        public Criteria andCtContactValueNotBetween(String value1, String value2) {
            addCriterion("ct_contact_value not between", value1, value2, "ctContactValue");
            return (Criteria) this;
        }

        public Criteria andCtRolesCodeIsNull() {
            addCriterion("ct_roles_code is null");
            return (Criteria) this;
        }

        public Criteria andCtRolesCodeIsNotNull() {
            addCriterion("ct_roles_code is not null");
            return (Criteria) this;
        }

        public Criteria andCtRolesCodeEqualTo(String value) {
            addCriterion("ct_roles_code =", value, "ctRolesCode");
            return (Criteria) this;
        }

        public Criteria andCtRolesCodeNotEqualTo(String value) {
            addCriterion("ct_roles_code <>", value, "ctRolesCode");
            return (Criteria) this;
        }

        public Criteria andCtRolesCodeGreaterThan(String value) {
            addCriterion("ct_roles_code >", value, "ctRolesCode");
            return (Criteria) this;
        }

        public Criteria andCtRolesCodeGreaterThanOrEqualTo(String value) {
            addCriterion("ct_roles_code >=", value, "ctRolesCode");
            return (Criteria) this;
        }

        public Criteria andCtRolesCodeLessThan(String value) {
            addCriterion("ct_roles_code <", value, "ctRolesCode");
            return (Criteria) this;
        }

        public Criteria andCtRolesCodeLessThanOrEqualTo(String value) {
            addCriterion("ct_roles_code <=", value, "ctRolesCode");
            return (Criteria) this;
        }

        public Criteria andCtRolesCodeLike(String value) {
            addCriterion("ct_roles_code like", value, "ctRolesCode");
            return (Criteria) this;
        }

        public Criteria andCtRolesCodeNotLike(String value) {
            addCriterion("ct_roles_code not like", value, "ctRolesCode");
            return (Criteria) this;
        }

        public Criteria andCtRolesCodeIn(List<String> values) {
            addCriterion("ct_roles_code in", values, "ctRolesCode");
            return (Criteria) this;
        }

        public Criteria andCtRolesCodeNotIn(List<String> values) {
            addCriterion("ct_roles_code not in", values, "ctRolesCode");
            return (Criteria) this;
        }

        public Criteria andCtRolesCodeBetween(String value1, String value2) {
            addCriterion("ct_roles_code between", value1, value2, "ctRolesCode");
            return (Criteria) this;
        }

        public Criteria andCtRolesCodeNotBetween(String value1, String value2) {
            addCriterion("ct_roles_code not between", value1, value2, "ctRolesCode");
            return (Criteria) this;
        }

        public Criteria andCtRolesDescIsNull() {
            addCriterion("ct_roles_desc is null");
            return (Criteria) this;
        }

        public Criteria andCtRolesDescIsNotNull() {
            addCriterion("ct_roles_desc is not null");
            return (Criteria) this;
        }

        public Criteria andCtRolesDescEqualTo(String value) {
            addCriterion("ct_roles_desc =", value, "ctRolesDesc");
            return (Criteria) this;
        }

        public Criteria andCtRolesDescNotEqualTo(String value) {
            addCriterion("ct_roles_desc <>", value, "ctRolesDesc");
            return (Criteria) this;
        }

        public Criteria andCtRolesDescGreaterThan(String value) {
            addCriterion("ct_roles_desc >", value, "ctRolesDesc");
            return (Criteria) this;
        }

        public Criteria andCtRolesDescGreaterThanOrEqualTo(String value) {
            addCriterion("ct_roles_desc >=", value, "ctRolesDesc");
            return (Criteria) this;
        }

        public Criteria andCtRolesDescLessThan(String value) {
            addCriterion("ct_roles_desc <", value, "ctRolesDesc");
            return (Criteria) this;
        }

        public Criteria andCtRolesDescLessThanOrEqualTo(String value) {
            addCriterion("ct_roles_desc <=", value, "ctRolesDesc");
            return (Criteria) this;
        }

        public Criteria andCtRolesDescLike(String value) {
            addCriterion("ct_roles_desc like", value, "ctRolesDesc");
            return (Criteria) this;
        }

        public Criteria andCtRolesDescNotLike(String value) {
            addCriterion("ct_roles_desc not like", value, "ctRolesDesc");
            return (Criteria) this;
        }

        public Criteria andCtRolesDescIn(List<String> values) {
            addCriterion("ct_roles_desc in", values, "ctRolesDesc");
            return (Criteria) this;
        }

        public Criteria andCtRolesDescNotIn(List<String> values) {
            addCriterion("ct_roles_desc not in", values, "ctRolesDesc");
            return (Criteria) this;
        }

        public Criteria andCtRolesDescBetween(String value1, String value2) {
            addCriterion("ct_roles_desc between", value1, value2, "ctRolesDesc");
            return (Criteria) this;
        }

        public Criteria andCtRolesDescNotBetween(String value1, String value2) {
            addCriterion("ct_roles_desc not between", value1, value2, "ctRolesDesc");
            return (Criteria) this;
        }

        public Criteria andCtNationalityIsNull() {
            addCriterion("ct_nationality is null");
            return (Criteria) this;
        }

        public Criteria andCtNationalityIsNotNull() {
            addCriterion("ct_nationality is not null");
            return (Criteria) this;
        }

        public Criteria andCtNationalityEqualTo(String value) {
            addCriterion("ct_nationality =", value, "ctNationality");
            return (Criteria) this;
        }

        public Criteria andCtNationalityNotEqualTo(String value) {
            addCriterion("ct_nationality <>", value, "ctNationality");
            return (Criteria) this;
        }

        public Criteria andCtNationalityGreaterThan(String value) {
            addCriterion("ct_nationality >", value, "ctNationality");
            return (Criteria) this;
        }

        public Criteria andCtNationalityGreaterThanOrEqualTo(String value) {
            addCriterion("ct_nationality >=", value, "ctNationality");
            return (Criteria) this;
        }

        public Criteria andCtNationalityLessThan(String value) {
            addCriterion("ct_nationality <", value, "ctNationality");
            return (Criteria) this;
        }

        public Criteria andCtNationalityLessThanOrEqualTo(String value) {
            addCriterion("ct_nationality <=", value, "ctNationality");
            return (Criteria) this;
        }

        public Criteria andCtNationalityLike(String value) {
            addCriterion("ct_nationality like", value, "ctNationality");
            return (Criteria) this;
        }

        public Criteria andCtNationalityNotLike(String value) {
            addCriterion("ct_nationality not like", value, "ctNationality");
            return (Criteria) this;
        }

        public Criteria andCtNationalityIn(List<String> values) {
            addCriterion("ct_nationality in", values, "ctNationality");
            return (Criteria) this;
        }

        public Criteria andCtNationalityNotIn(List<String> values) {
            addCriterion("ct_nationality not in", values, "ctNationality");
            return (Criteria) this;
        }

        public Criteria andCtNationalityBetween(String value1, String value2) {
            addCriterion("ct_nationality between", value1, value2, "ctNationality");
            return (Criteria) this;
        }

        public Criteria andCtNationalityNotBetween(String value1, String value2) {
            addCriterion("ct_nationality not between", value1, value2, "ctNationality");
            return (Criteria) this;
        }

        public Criteria andCtRaceIsNull() {
            addCriterion("ct_race is null");
            return (Criteria) this;
        }

        public Criteria andCtRaceIsNotNull() {
            addCriterion("ct_race is not null");
            return (Criteria) this;
        }

        public Criteria andCtRaceEqualTo(String value) {
            addCriterion("ct_race =", value, "ctRace");
            return (Criteria) this;
        }

        public Criteria andCtRaceNotEqualTo(String value) {
            addCriterion("ct_race <>", value, "ctRace");
            return (Criteria) this;
        }

        public Criteria andCtRaceGreaterThan(String value) {
            addCriterion("ct_race >", value, "ctRace");
            return (Criteria) this;
        }

        public Criteria andCtRaceGreaterThanOrEqualTo(String value) {
            addCriterion("ct_race >=", value, "ctRace");
            return (Criteria) this;
        }

        public Criteria andCtRaceLessThan(String value) {
            addCriterion("ct_race <", value, "ctRace");
            return (Criteria) this;
        }

        public Criteria andCtRaceLessThanOrEqualTo(String value) {
            addCriterion("ct_race <=", value, "ctRace");
            return (Criteria) this;
        }

        public Criteria andCtRaceLike(String value) {
            addCriterion("ct_race like", value, "ctRace");
            return (Criteria) this;
        }

        public Criteria andCtRaceNotLike(String value) {
            addCriterion("ct_race not like", value, "ctRace");
            return (Criteria) this;
        }

        public Criteria andCtRaceIn(List<String> values) {
            addCriterion("ct_race in", values, "ctRace");
            return (Criteria) this;
        }

        public Criteria andCtRaceNotIn(List<String> values) {
            addCriterion("ct_race not in", values, "ctRace");
            return (Criteria) this;
        }

        public Criteria andCtRaceBetween(String value1, String value2) {
            addCriterion("ct_race between", value1, value2, "ctRace");
            return (Criteria) this;
        }

        public Criteria andCtRaceNotBetween(String value1, String value2) {
            addCriterion("ct_race not between", value1, value2, "ctRace");
            return (Criteria) this;
        }

        public Criteria andCtAddressIdIsNull() {
            addCriterion("ct_address_id is null");
            return (Criteria) this;
        }

        public Criteria andCtAddressIdIsNotNull() {
            addCriterion("ct_address_id is not null");
            return (Criteria) this;
        }

        public Criteria andCtAddressIdEqualTo(String value) {
            addCriterion("ct_address_id =", value, "ctAddressId");
            return (Criteria) this;
        }

        public Criteria andCtAddressIdNotEqualTo(String value) {
            addCriterion("ct_address_id <>", value, "ctAddressId");
            return (Criteria) this;
        }

        public Criteria andCtAddressIdGreaterThan(String value) {
            addCriterion("ct_address_id >", value, "ctAddressId");
            return (Criteria) this;
        }

        public Criteria andCtAddressIdGreaterThanOrEqualTo(String value) {
            addCriterion("ct_address_id >=", value, "ctAddressId");
            return (Criteria) this;
        }

        public Criteria andCtAddressIdLessThan(String value) {
            addCriterion("ct_address_id <", value, "ctAddressId");
            return (Criteria) this;
        }

        public Criteria andCtAddressIdLessThanOrEqualTo(String value) {
            addCriterion("ct_address_id <=", value, "ctAddressId");
            return (Criteria) this;
        }

        public Criteria andCtAddressIdLike(String value) {
            addCriterion("ct_address_id like", value, "ctAddressId");
            return (Criteria) this;
        }

        public Criteria andCtAddressIdNotLike(String value) {
            addCriterion("ct_address_id not like", value, "ctAddressId");
            return (Criteria) this;
        }

        public Criteria andCtAddressIdIn(List<String> values) {
            addCriterion("ct_address_id in", values, "ctAddressId");
            return (Criteria) this;
        }

        public Criteria andCtAddressIdNotIn(List<String> values) {
            addCriterion("ct_address_id not in", values, "ctAddressId");
            return (Criteria) this;
        }

        public Criteria andCtAddressIdBetween(String value1, String value2) {
            addCriterion("ct_address_id between", value1, value2, "ctAddressId");
            return (Criteria) this;
        }

        public Criteria andCtAddressIdNotBetween(String value1, String value2) {
            addCriterion("ct_address_id not between", value1, value2, "ctAddressId");
            return (Criteria) this;
        }

        public Criteria andCtPrimaryaddressIsNull() {
            addCriterion("ct_primaryAddress is null");
            return (Criteria) this;
        }

        public Criteria andCtPrimaryaddressIsNotNull() {
            addCriterion("ct_primaryAddress is not null");
            return (Criteria) this;
        }

        public Criteria andCtPrimaryaddressEqualTo(Boolean value) {
            addCriterion("ct_primaryAddress =", value, "ctPrimaryaddress");
            return (Criteria) this;
        }

        public Criteria andCtPrimaryaddressNotEqualTo(Boolean value) {
            addCriterion("ct_primaryAddress <>", value, "ctPrimaryaddress");
            return (Criteria) this;
        }

        public Criteria andCtPrimaryaddressGreaterThan(Boolean value) {
            addCriterion("ct_primaryAddress >", value, "ctPrimaryaddress");
            return (Criteria) this;
        }

        public Criteria andCtPrimaryaddressGreaterThanOrEqualTo(Boolean value) {
            addCriterion("ct_primaryAddress >=", value, "ctPrimaryaddress");
            return (Criteria) this;
        }

        public Criteria andCtPrimaryaddressLessThan(Boolean value) {
            addCriterion("ct_primaryAddress <", value, "ctPrimaryaddress");
            return (Criteria) this;
        }

        public Criteria andCtPrimaryaddressLessThanOrEqualTo(Boolean value) {
            addCriterion("ct_primaryAddress <=", value, "ctPrimaryaddress");
            return (Criteria) this;
        }

        public Criteria andCtPrimaryaddressIn(List<Boolean> values) {
            addCriterion("ct_primaryAddress in", values, "ctPrimaryaddress");
            return (Criteria) this;
        }

        public Criteria andCtPrimaryaddressNotIn(List<Boolean> values) {
            addCriterion("ct_primaryAddress not in", values, "ctPrimaryaddress");
            return (Criteria) this;
        }

        public Criteria andCtPrimaryaddressBetween(Boolean value1, Boolean value2) {
            addCriterion("ct_primaryAddress between", value1, value2, "ctPrimaryaddress");
            return (Criteria) this;
        }

        public Criteria andCtPrimaryaddressNotBetween(Boolean value1, Boolean value2) {
            addCriterion("ct_primaryAddress not between", value1, value2, "ctPrimaryaddress");
            return (Criteria) this;
        }

        public Criteria andCtLine1IsNull() {
            addCriterion("ct_line1 is null");
            return (Criteria) this;
        }

        public Criteria andCtLine1IsNotNull() {
            addCriterion("ct_line1 is not null");
            return (Criteria) this;
        }

        public Criteria andCtLine1EqualTo(String value) {
            addCriterion("ct_line1 =", value, "ctLine1");
            return (Criteria) this;
        }

        public Criteria andCtLine1NotEqualTo(String value) {
            addCriterion("ct_line1 <>", value, "ctLine1");
            return (Criteria) this;
        }

        public Criteria andCtLine1GreaterThan(String value) {
            addCriterion("ct_line1 >", value, "ctLine1");
            return (Criteria) this;
        }

        public Criteria andCtLine1GreaterThanOrEqualTo(String value) {
            addCriterion("ct_line1 >=", value, "ctLine1");
            return (Criteria) this;
        }

        public Criteria andCtLine1LessThan(String value) {
            addCriterion("ct_line1 <", value, "ctLine1");
            return (Criteria) this;
        }

        public Criteria andCtLine1LessThanOrEqualTo(String value) {
            addCriterion("ct_line1 <=", value, "ctLine1");
            return (Criteria) this;
        }

        public Criteria andCtLine1Like(String value) {
            addCriterion("ct_line1 like", value, "ctLine1");
            return (Criteria) this;
        }

        public Criteria andCtLine1NotLike(String value) {
            addCriterion("ct_line1 not like", value, "ctLine1");
            return (Criteria) this;
        }

        public Criteria andCtLine1In(List<String> values) {
            addCriterion("ct_line1 in", values, "ctLine1");
            return (Criteria) this;
        }

        public Criteria andCtLine1NotIn(List<String> values) {
            addCriterion("ct_line1 not in", values, "ctLine1");
            return (Criteria) this;
        }

        public Criteria andCtLine1Between(String value1, String value2) {
            addCriterion("ct_line1 between", value1, value2, "ctLine1");
            return (Criteria) this;
        }

        public Criteria andCtLine1NotBetween(String value1, String value2) {
            addCriterion("ct_line1 not between", value1, value2, "ctLine1");
            return (Criteria) this;
        }

        public Criteria andCtLine2IsNull() {
            addCriterion("ct_line2 is null");
            return (Criteria) this;
        }

        public Criteria andCtLine2IsNotNull() {
            addCriterion("ct_line2 is not null");
            return (Criteria) this;
        }

        public Criteria andCtLine2EqualTo(String value) {
            addCriterion("ct_line2 =", value, "ctLine2");
            return (Criteria) this;
        }

        public Criteria andCtLine2NotEqualTo(String value) {
            addCriterion("ct_line2 <>", value, "ctLine2");
            return (Criteria) this;
        }

        public Criteria andCtLine2GreaterThan(String value) {
            addCriterion("ct_line2 >", value, "ctLine2");
            return (Criteria) this;
        }

        public Criteria andCtLine2GreaterThanOrEqualTo(String value) {
            addCriterion("ct_line2 >=", value, "ctLine2");
            return (Criteria) this;
        }

        public Criteria andCtLine2LessThan(String value) {
            addCriterion("ct_line2 <", value, "ctLine2");
            return (Criteria) this;
        }

        public Criteria andCtLine2LessThanOrEqualTo(String value) {
            addCriterion("ct_line2 <=", value, "ctLine2");
            return (Criteria) this;
        }

        public Criteria andCtLine2Like(String value) {
            addCriterion("ct_line2 like", value, "ctLine2");
            return (Criteria) this;
        }

        public Criteria andCtLine2NotLike(String value) {
            addCriterion("ct_line2 not like", value, "ctLine2");
            return (Criteria) this;
        }

        public Criteria andCtLine2In(List<String> values) {
            addCriterion("ct_line2 in", values, "ctLine2");
            return (Criteria) this;
        }

        public Criteria andCtLine2NotIn(List<String> values) {
            addCriterion("ct_line2 not in", values, "ctLine2");
            return (Criteria) this;
        }

        public Criteria andCtLine2Between(String value1, String value2) {
            addCriterion("ct_line2 between", value1, value2, "ctLine2");
            return (Criteria) this;
        }

        public Criteria andCtLine2NotBetween(String value1, String value2) {
            addCriterion("ct_line2 not between", value1, value2, "ctLine2");
            return (Criteria) this;
        }

        public Criteria andCtLine3IsNull() {
            addCriterion("ct_line3 is null");
            return (Criteria) this;
        }

        public Criteria andCtLine3IsNotNull() {
            addCriterion("ct_line3 is not null");
            return (Criteria) this;
        }

        public Criteria andCtLine3EqualTo(String value) {
            addCriterion("ct_line3 =", value, "ctLine3");
            return (Criteria) this;
        }

        public Criteria andCtLine3NotEqualTo(String value) {
            addCriterion("ct_line3 <>", value, "ctLine3");
            return (Criteria) this;
        }

        public Criteria andCtLine3GreaterThan(String value) {
            addCriterion("ct_line3 >", value, "ctLine3");
            return (Criteria) this;
        }

        public Criteria andCtLine3GreaterThanOrEqualTo(String value) {
            addCriterion("ct_line3 >=", value, "ctLine3");
            return (Criteria) this;
        }

        public Criteria andCtLine3LessThan(String value) {
            addCriterion("ct_line3 <", value, "ctLine3");
            return (Criteria) this;
        }

        public Criteria andCtLine3LessThanOrEqualTo(String value) {
            addCriterion("ct_line3 <=", value, "ctLine3");
            return (Criteria) this;
        }

        public Criteria andCtLine3Like(String value) {
            addCriterion("ct_line3 like", value, "ctLine3");
            return (Criteria) this;
        }

        public Criteria andCtLine3NotLike(String value) {
            addCriterion("ct_line3 not like", value, "ctLine3");
            return (Criteria) this;
        }

        public Criteria andCtLine3In(List<String> values) {
            addCriterion("ct_line3 in", values, "ctLine3");
            return (Criteria) this;
        }

        public Criteria andCtLine3NotIn(List<String> values) {
            addCriterion("ct_line3 not in", values, "ctLine3");
            return (Criteria) this;
        }

        public Criteria andCtLine3Between(String value1, String value2) {
            addCriterion("ct_line3 between", value1, value2, "ctLine3");
            return (Criteria) this;
        }

        public Criteria andCtLine3NotBetween(String value1, String value2) {
            addCriterion("ct_line3 not between", value1, value2, "ctLine3");
            return (Criteria) this;
        }

        public Criteria andCtPostalcodeIsNull() {
            addCriterion("ct_postalCode is null");
            return (Criteria) this;
        }

        public Criteria andCtPostalcodeIsNotNull() {
            addCriterion("ct_postalCode is not null");
            return (Criteria) this;
        }

        public Criteria andCtPostalcodeEqualTo(String value) {
            addCriterion("ct_postalCode =", value, "ctPostalcode");
            return (Criteria) this;
        }

        public Criteria andCtPostalcodeNotEqualTo(String value) {
            addCriterion("ct_postalCode <>", value, "ctPostalcode");
            return (Criteria) this;
        }

        public Criteria andCtPostalcodeGreaterThan(String value) {
            addCriterion("ct_postalCode >", value, "ctPostalcode");
            return (Criteria) this;
        }

        public Criteria andCtPostalcodeGreaterThanOrEqualTo(String value) {
            addCriterion("ct_postalCode >=", value, "ctPostalcode");
            return (Criteria) this;
        }

        public Criteria andCtPostalcodeLessThan(String value) {
            addCriterion("ct_postalCode <", value, "ctPostalcode");
            return (Criteria) this;
        }

        public Criteria andCtPostalcodeLessThanOrEqualTo(String value) {
            addCriterion("ct_postalCode <=", value, "ctPostalcode");
            return (Criteria) this;
        }

        public Criteria andCtPostalcodeLike(String value) {
            addCriterion("ct_postalCode like", value, "ctPostalcode");
            return (Criteria) this;
        }

        public Criteria andCtPostalcodeNotLike(String value) {
            addCriterion("ct_postalCode not like", value, "ctPostalcode");
            return (Criteria) this;
        }

        public Criteria andCtPostalcodeIn(List<String> values) {
            addCriterion("ct_postalCode in", values, "ctPostalcode");
            return (Criteria) this;
        }

        public Criteria andCtPostalcodeNotIn(List<String> values) {
            addCriterion("ct_postalCode not in", values, "ctPostalcode");
            return (Criteria) this;
        }

        public Criteria andCtPostalcodeBetween(String value1, String value2) {
            addCriterion("ct_postalCode between", value1, value2, "ctPostalcode");
            return (Criteria) this;
        }

        public Criteria andCtPostalcodeNotBetween(String value1, String value2) {
            addCriterion("ct_postalCode not between", value1, value2, "ctPostalcode");
            return (Criteria) this;
        }

        public Criteria andCtCityIsNull() {
            addCriterion("ct_city is null");
            return (Criteria) this;
        }

        public Criteria andCtCityIsNotNull() {
            addCriterion("ct_city is not null");
            return (Criteria) this;
        }

        public Criteria andCtCityEqualTo(String value) {
            addCriterion("ct_city =", value, "ctCity");
            return (Criteria) this;
        }

        public Criteria andCtCityNotEqualTo(String value) {
            addCriterion("ct_city <>", value, "ctCity");
            return (Criteria) this;
        }

        public Criteria andCtCityGreaterThan(String value) {
            addCriterion("ct_city >", value, "ctCity");
            return (Criteria) this;
        }

        public Criteria andCtCityGreaterThanOrEqualTo(String value) {
            addCriterion("ct_city >=", value, "ctCity");
            return (Criteria) this;
        }

        public Criteria andCtCityLessThan(String value) {
            addCriterion("ct_city <", value, "ctCity");
            return (Criteria) this;
        }

        public Criteria andCtCityLessThanOrEqualTo(String value) {
            addCriterion("ct_city <=", value, "ctCity");
            return (Criteria) this;
        }

        public Criteria andCtCityLike(String value) {
            addCriterion("ct_city like", value, "ctCity");
            return (Criteria) this;
        }

        public Criteria andCtCityNotLike(String value) {
            addCriterion("ct_city not like", value, "ctCity");
            return (Criteria) this;
        }

        public Criteria andCtCityIn(List<String> values) {
            addCriterion("ct_city in", values, "ctCity");
            return (Criteria) this;
        }

        public Criteria andCtCityNotIn(List<String> values) {
            addCriterion("ct_city not in", values, "ctCity");
            return (Criteria) this;
        }

        public Criteria andCtCityBetween(String value1, String value2) {
            addCriterion("ct_city between", value1, value2, "ctCity");
            return (Criteria) this;
        }

        public Criteria andCtCityNotBetween(String value1, String value2) {
            addCriterion("ct_city not between", value1, value2, "ctCity");
            return (Criteria) this;
        }

        public Criteria andCtStateIsNull() {
            addCriterion("ct_state is null");
            return (Criteria) this;
        }

        public Criteria andCtStateIsNotNull() {
            addCriterion("ct_state is not null");
            return (Criteria) this;
        }

        public Criteria andCtStateEqualTo(String value) {
            addCriterion("ct_state =", value, "ctState");
            return (Criteria) this;
        }

        public Criteria andCtStateNotEqualTo(String value) {
            addCriterion("ct_state <>", value, "ctState");
            return (Criteria) this;
        }

        public Criteria andCtStateGreaterThan(String value) {
            addCriterion("ct_state >", value, "ctState");
            return (Criteria) this;
        }

        public Criteria andCtStateGreaterThanOrEqualTo(String value) {
            addCriterion("ct_state >=", value, "ctState");
            return (Criteria) this;
        }

        public Criteria andCtStateLessThan(String value) {
            addCriterion("ct_state <", value, "ctState");
            return (Criteria) this;
        }

        public Criteria andCtStateLessThanOrEqualTo(String value) {
            addCriterion("ct_state <=", value, "ctState");
            return (Criteria) this;
        }

        public Criteria andCtStateLike(String value) {
            addCriterion("ct_state like", value, "ctState");
            return (Criteria) this;
        }

        public Criteria andCtStateNotLike(String value) {
            addCriterion("ct_state not like", value, "ctState");
            return (Criteria) this;
        }

        public Criteria andCtStateIn(List<String> values) {
            addCriterion("ct_state in", values, "ctState");
            return (Criteria) this;
        }

        public Criteria andCtStateNotIn(List<String> values) {
            addCriterion("ct_state not in", values, "ctState");
            return (Criteria) this;
        }

        public Criteria andCtStateBetween(String value1, String value2) {
            addCriterion("ct_state between", value1, value2, "ctState");
            return (Criteria) this;
        }

        public Criteria andCtStateNotBetween(String value1, String value2) {
            addCriterion("ct_state not between", value1, value2, "ctState");
            return (Criteria) this;
        }

        public Criteria andCtCountryIsNull() {
            addCriterion("ct_country is null");
            return (Criteria) this;
        }

        public Criteria andCtCountryIsNotNull() {
            addCriterion("ct_country is not null");
            return (Criteria) this;
        }

        public Criteria andCtCountryEqualTo(String value) {
            addCriterion("ct_country =", value, "ctCountry");
            return (Criteria) this;
        }

        public Criteria andCtCountryNotEqualTo(String value) {
            addCriterion("ct_country <>", value, "ctCountry");
            return (Criteria) this;
        }

        public Criteria andCtCountryGreaterThan(String value) {
            addCriterion("ct_country >", value, "ctCountry");
            return (Criteria) this;
        }

        public Criteria andCtCountryGreaterThanOrEqualTo(String value) {
            addCriterion("ct_country >=", value, "ctCountry");
            return (Criteria) this;
        }

        public Criteria andCtCountryLessThan(String value) {
            addCriterion("ct_country <", value, "ctCountry");
            return (Criteria) this;
        }

        public Criteria andCtCountryLessThanOrEqualTo(String value) {
            addCriterion("ct_country <=", value, "ctCountry");
            return (Criteria) this;
        }

        public Criteria andCtCountryLike(String value) {
            addCriterion("ct_country like", value, "ctCountry");
            return (Criteria) this;
        }

        public Criteria andCtCountryNotLike(String value) {
            addCriterion("ct_country not like", value, "ctCountry");
            return (Criteria) this;
        }

        public Criteria andCtCountryIn(List<String> values) {
            addCriterion("ct_country in", values, "ctCountry");
            return (Criteria) this;
        }

        public Criteria andCtCountryNotIn(List<String> values) {
            addCriterion("ct_country not in", values, "ctCountry");
            return (Criteria) this;
        }

        public Criteria andCtCountryBetween(String value1, String value2) {
            addCriterion("ct_country between", value1, value2, "ctCountry");
            return (Criteria) this;
        }

        public Criteria andCtCountryNotBetween(String value1, String value2) {
            addCriterion("ct_country not between", value1, value2, "ctCountry");
            return (Criteria) this;
        }

        public Criteria andCtParam1IsNull() {
            addCriterion("ct_param1 is null");
            return (Criteria) this;
        }

        public Criteria andCtParam1IsNotNull() {
            addCriterion("ct_param1 is not null");
            return (Criteria) this;
        }

        public Criteria andCtParam1EqualTo(String value) {
            addCriterion("ct_param1 =", value, "ctParam1");
            return (Criteria) this;
        }

        public Criteria andCtParam1NotEqualTo(String value) {
            addCriterion("ct_param1 <>", value, "ctParam1");
            return (Criteria) this;
        }

        public Criteria andCtParam1GreaterThan(String value) {
            addCriterion("ct_param1 >", value, "ctParam1");
            return (Criteria) this;
        }

        public Criteria andCtParam1GreaterThanOrEqualTo(String value) {
            addCriterion("ct_param1 >=", value, "ctParam1");
            return (Criteria) this;
        }

        public Criteria andCtParam1LessThan(String value) {
            addCriterion("ct_param1 <", value, "ctParam1");
            return (Criteria) this;
        }

        public Criteria andCtParam1LessThanOrEqualTo(String value) {
            addCriterion("ct_param1 <=", value, "ctParam1");
            return (Criteria) this;
        }

        public Criteria andCtParam1Like(String value) {
            addCriterion("ct_param1 like", value, "ctParam1");
            return (Criteria) this;
        }

        public Criteria andCtParam1NotLike(String value) {
            addCriterion("ct_param1 not like", value, "ctParam1");
            return (Criteria) this;
        }

        public Criteria andCtParam1In(List<String> values) {
            addCriterion("ct_param1 in", values, "ctParam1");
            return (Criteria) this;
        }

        public Criteria andCtParam1NotIn(List<String> values) {
            addCriterion("ct_param1 not in", values, "ctParam1");
            return (Criteria) this;
        }

        public Criteria andCtParam1Between(String value1, String value2) {
            addCriterion("ct_param1 between", value1, value2, "ctParam1");
            return (Criteria) this;
        }

        public Criteria andCtParam1NotBetween(String value1, String value2) {
            addCriterion("ct_param1 not between", value1, value2, "ctParam1");
            return (Criteria) this;
        }

        public Criteria andCtParam2IsNull() {
            addCriterion("ct_param2 is null");
            return (Criteria) this;
        }

        public Criteria andCtParam2IsNotNull() {
            addCriterion("ct_param2 is not null");
            return (Criteria) this;
        }

        public Criteria andCtParam2EqualTo(String value) {
            addCriterion("ct_param2 =", value, "ctParam2");
            return (Criteria) this;
        }

        public Criteria andCtParam2NotEqualTo(String value) {
            addCriterion("ct_param2 <>", value, "ctParam2");
            return (Criteria) this;
        }

        public Criteria andCtParam2GreaterThan(String value) {
            addCriterion("ct_param2 >", value, "ctParam2");
            return (Criteria) this;
        }

        public Criteria andCtParam2GreaterThanOrEqualTo(String value) {
            addCriterion("ct_param2 >=", value, "ctParam2");
            return (Criteria) this;
        }

        public Criteria andCtParam2LessThan(String value) {
            addCriterion("ct_param2 <", value, "ctParam2");
            return (Criteria) this;
        }

        public Criteria andCtParam2LessThanOrEqualTo(String value) {
            addCriterion("ct_param2 <=", value, "ctParam2");
            return (Criteria) this;
        }

        public Criteria andCtParam2Like(String value) {
            addCriterion("ct_param2 like", value, "ctParam2");
            return (Criteria) this;
        }

        public Criteria andCtParam2NotLike(String value) {
            addCriterion("ct_param2 not like", value, "ctParam2");
            return (Criteria) this;
        }

        public Criteria andCtParam2In(List<String> values) {
            addCriterion("ct_param2 in", values, "ctParam2");
            return (Criteria) this;
        }

        public Criteria andCtParam2NotIn(List<String> values) {
            addCriterion("ct_param2 not in", values, "ctParam2");
            return (Criteria) this;
        }

        public Criteria andCtParam2Between(String value1, String value2) {
            addCriterion("ct_param2 between", value1, value2, "ctParam2");
            return (Criteria) this;
        }

        public Criteria andCtParam2NotBetween(String value1, String value2) {
            addCriterion("ct_param2 not between", value1, value2, "ctParam2");
            return (Criteria) this;
        }

        public Criteria andCtParam3IsNull() {
            addCriterion("ct_param3 is null");
            return (Criteria) this;
        }

        public Criteria andCtParam3IsNotNull() {
            addCriterion("ct_param3 is not null");
            return (Criteria) this;
        }

        public Criteria andCtParam3EqualTo(String value) {
            addCriterion("ct_param3 =", value, "ctParam3");
            return (Criteria) this;
        }

        public Criteria andCtParam3NotEqualTo(String value) {
            addCriterion("ct_param3 <>", value, "ctParam3");
            return (Criteria) this;
        }

        public Criteria andCtParam3GreaterThan(String value) {
            addCriterion("ct_param3 >", value, "ctParam3");
            return (Criteria) this;
        }

        public Criteria andCtParam3GreaterThanOrEqualTo(String value) {
            addCriterion("ct_param3 >=", value, "ctParam3");
            return (Criteria) this;
        }

        public Criteria andCtParam3LessThan(String value) {
            addCriterion("ct_param3 <", value, "ctParam3");
            return (Criteria) this;
        }

        public Criteria andCtParam3LessThanOrEqualTo(String value) {
            addCriterion("ct_param3 <=", value, "ctParam3");
            return (Criteria) this;
        }

        public Criteria andCtParam3Like(String value) {
            addCriterion("ct_param3 like", value, "ctParam3");
            return (Criteria) this;
        }

        public Criteria andCtParam3NotLike(String value) {
            addCriterion("ct_param3 not like", value, "ctParam3");
            return (Criteria) this;
        }

        public Criteria andCtParam3In(List<String> values) {
            addCriterion("ct_param3 in", values, "ctParam3");
            return (Criteria) this;
        }

        public Criteria andCtParam3NotIn(List<String> values) {
            addCriterion("ct_param3 not in", values, "ctParam3");
            return (Criteria) this;
        }

        public Criteria andCtParam3Between(String value1, String value2) {
            addCriterion("ct_param3 between", value1, value2, "ctParam3");
            return (Criteria) this;
        }

        public Criteria andCtParam3NotBetween(String value1, String value2) {
            addCriterion("ct_param3 not between", value1, value2, "ctParam3");
            return (Criteria) this;
        }

        public Criteria andCtParam4IsNull() {
            addCriterion("ct_param4 is null");
            return (Criteria) this;
        }

        public Criteria andCtParam4IsNotNull() {
            addCriterion("ct_param4 is not null");
            return (Criteria) this;
        }

        public Criteria andCtParam4EqualTo(String value) {
            addCriterion("ct_param4 =", value, "ctParam4");
            return (Criteria) this;
        }

        public Criteria andCtParam4NotEqualTo(String value) {
            addCriterion("ct_param4 <>", value, "ctParam4");
            return (Criteria) this;
        }

        public Criteria andCtParam4GreaterThan(String value) {
            addCriterion("ct_param4 >", value, "ctParam4");
            return (Criteria) this;
        }

        public Criteria andCtParam4GreaterThanOrEqualTo(String value) {
            addCriterion("ct_param4 >=", value, "ctParam4");
            return (Criteria) this;
        }

        public Criteria andCtParam4LessThan(String value) {
            addCriterion("ct_param4 <", value, "ctParam4");
            return (Criteria) this;
        }

        public Criteria andCtParam4LessThanOrEqualTo(String value) {
            addCriterion("ct_param4 <=", value, "ctParam4");
            return (Criteria) this;
        }

        public Criteria andCtParam4Like(String value) {
            addCriterion("ct_param4 like", value, "ctParam4");
            return (Criteria) this;
        }

        public Criteria andCtParam4NotLike(String value) {
            addCriterion("ct_param4 not like", value, "ctParam4");
            return (Criteria) this;
        }

        public Criteria andCtParam4In(List<String> values) {
            addCriterion("ct_param4 in", values, "ctParam4");
            return (Criteria) this;
        }

        public Criteria andCtParam4NotIn(List<String> values) {
            addCriterion("ct_param4 not in", values, "ctParam4");
            return (Criteria) this;
        }

        public Criteria andCtParam4Between(String value1, String value2) {
            addCriterion("ct_param4 between", value1, value2, "ctParam4");
            return (Criteria) this;
        }

        public Criteria andCtParam4NotBetween(String value1, String value2) {
            addCriterion("ct_param4 not between", value1, value2, "ctParam4");
            return (Criteria) this;
        }

        public Criteria andCtParam5IsNull() {
            addCriterion("ct_param5 is null");
            return (Criteria) this;
        }

        public Criteria andCtParam5IsNotNull() {
            addCriterion("ct_param5 is not null");
            return (Criteria) this;
        }

        public Criteria andCtParam5EqualTo(String value) {
            addCriterion("ct_param5 =", value, "ctParam5");
            return (Criteria) this;
        }

        public Criteria andCtParam5NotEqualTo(String value) {
            addCriterion("ct_param5 <>", value, "ctParam5");
            return (Criteria) this;
        }

        public Criteria andCtParam5GreaterThan(String value) {
            addCriterion("ct_param5 >", value, "ctParam5");
            return (Criteria) this;
        }

        public Criteria andCtParam5GreaterThanOrEqualTo(String value) {
            addCriterion("ct_param5 >=", value, "ctParam5");
            return (Criteria) this;
        }

        public Criteria andCtParam5LessThan(String value) {
            addCriterion("ct_param5 <", value, "ctParam5");
            return (Criteria) this;
        }

        public Criteria andCtParam5LessThanOrEqualTo(String value) {
            addCriterion("ct_param5 <=", value, "ctParam5");
            return (Criteria) this;
        }

        public Criteria andCtParam5Like(String value) {
            addCriterion("ct_param5 like", value, "ctParam5");
            return (Criteria) this;
        }

        public Criteria andCtParam5NotLike(String value) {
            addCriterion("ct_param5 not like", value, "ctParam5");
            return (Criteria) this;
        }

        public Criteria andCtParam5In(List<String> values) {
            addCriterion("ct_param5 in", values, "ctParam5");
            return (Criteria) this;
        }

        public Criteria andCtParam5NotIn(List<String> values) {
            addCriterion("ct_param5 not in", values, "ctParam5");
            return (Criteria) this;
        }

        public Criteria andCtParam5Between(String value1, String value2) {
            addCriterion("ct_param5 between", value1, value2, "ctParam5");
            return (Criteria) this;
        }

        public Criteria andCtParam5NotBetween(String value1, String value2) {
            addCriterion("ct_param5 not between", value1, value2, "ctParam5");
            return (Criteria) this;
        }

        public Criteria andCtCreatedDatetimeIsNull() {
            addCriterion("ct_created_datetime is null");
            return (Criteria) this;
        }

        public Criteria andCtCreatedDatetimeIsNotNull() {
            addCriterion("ct_created_datetime is not null");
            return (Criteria) this;
        }

        public Criteria andCtCreatedDatetimeEqualTo(Date value) {
            addCriterion("ct_created_datetime =", value, "ctCreatedDatetime");
            return (Criteria) this;
        }

        public Criteria andCtCreatedDatetimeNotEqualTo(Date value) {
            addCriterion("ct_created_datetime <>", value, "ctCreatedDatetime");
            return (Criteria) this;
        }

        public Criteria andCtCreatedDatetimeGreaterThan(Date value) {
            addCriterion("ct_created_datetime >", value, "ctCreatedDatetime");
            return (Criteria) this;
        }

        public Criteria andCtCreatedDatetimeGreaterThanOrEqualTo(Date value) {
            addCriterion("ct_created_datetime >=", value, "ctCreatedDatetime");
            return (Criteria) this;
        }

        public Criteria andCtCreatedDatetimeLessThan(Date value) {
            addCriterion("ct_created_datetime <", value, "ctCreatedDatetime");
            return (Criteria) this;
        }

        public Criteria andCtCreatedDatetimeLessThanOrEqualTo(Date value) {
            addCriterion("ct_created_datetime <=", value, "ctCreatedDatetime");
            return (Criteria) this;
        }

        public Criteria andCtCreatedDatetimeIn(List<Date> values) {
            addCriterion("ct_created_datetime in", values, "ctCreatedDatetime");
            return (Criteria) this;
        }

        public Criteria andCtCreatedDatetimeNotIn(List<Date> values) {
            addCriterion("ct_created_datetime not in", values, "ctCreatedDatetime");
            return (Criteria) this;
        }

        public Criteria andCtCreatedDatetimeBetween(Date value1, Date value2) {
            addCriterion("ct_created_datetime between", value1, value2, "ctCreatedDatetime");
            return (Criteria) this;
        }

        public Criteria andCtCreatedDatetimeNotBetween(Date value1, Date value2) {
            addCriterion("ct_created_datetime not between", value1, value2, "ctCreatedDatetime");
            return (Criteria) this;
        }

        public Criteria andCtUpdatedDatetimeIsNull() {
            addCriterion("ct_updated_datetime is null");
            return (Criteria) this;
        }

        public Criteria andCtUpdatedDatetimeIsNotNull() {
            addCriterion("ct_updated_datetime is not null");
            return (Criteria) this;
        }

        public Criteria andCtUpdatedDatetimeEqualTo(Date value) {
            addCriterion("ct_updated_datetime =", value, "ctUpdatedDatetime");
            return (Criteria) this;
        }

        public Criteria andCtUpdatedDatetimeNotEqualTo(Date value) {
            addCriterion("ct_updated_datetime <>", value, "ctUpdatedDatetime");
            return (Criteria) this;
        }

        public Criteria andCtUpdatedDatetimeGreaterThan(Date value) {
            addCriterion("ct_updated_datetime >", value, "ctUpdatedDatetime");
            return (Criteria) this;
        }

        public Criteria andCtUpdatedDatetimeGreaterThanOrEqualTo(Date value) {
            addCriterion("ct_updated_datetime >=", value, "ctUpdatedDatetime");
            return (Criteria) this;
        }

        public Criteria andCtUpdatedDatetimeLessThan(Date value) {
            addCriterion("ct_updated_datetime <", value, "ctUpdatedDatetime");
            return (Criteria) this;
        }

        public Criteria andCtUpdatedDatetimeLessThanOrEqualTo(Date value) {
            addCriterion("ct_updated_datetime <=", value, "ctUpdatedDatetime");
            return (Criteria) this;
        }

        public Criteria andCtUpdatedDatetimeIn(List<Date> values) {
            addCriterion("ct_updated_datetime in", values, "ctUpdatedDatetime");
            return (Criteria) this;
        }

        public Criteria andCtUpdatedDatetimeNotIn(List<Date> values) {
            addCriterion("ct_updated_datetime not in", values, "ctUpdatedDatetime");
            return (Criteria) this;
        }

        public Criteria andCtUpdatedDatetimeBetween(Date value1, Date value2) {
            addCriterion("ct_updated_datetime between", value1, value2, "ctUpdatedDatetime");
            return (Criteria) this;
        }

        public Criteria andCtUpdatedDatetimeNotBetween(Date value1, Date value2) {
            addCriterion("ct_updated_datetime not between", value1, value2, "ctUpdatedDatetime");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}