package com.test.bean;

public class RoleBean {
    private Integer rtId;

    private Integer rtCtId;

    private String rtCode;

    private String rtDescription;

    private String rtParam1;

    private String rtParam2;

    private String rtParam3;

    private String rtParam4;

    private String rtParam5;

    public Integer getRtId() {
        return rtId;
    }

    public void setRtId(Integer rtId) {
        this.rtId = rtId;
    }

    public Integer getRtCtId() {
        return rtCtId;
    }

    public void setRtCtId(Integer rtCtId) {
        this.rtCtId = rtCtId;
    }

    public String getRtCode() {
        return rtCode;
    }

    public void setRtCode(String rtCode) {
        this.rtCode = rtCode == null ? null : rtCode.trim();
    }

    public String getRtDescription() {
        return rtDescription;
    }

    public void setRtDescription(String rtDescription) {
        this.rtDescription = rtDescription == null ? null : rtDescription.trim();
    }

    public String getRtParam1() {
        return rtParam1;
    }

    public void setRtParam1(String rtParam1) {
        this.rtParam1 = rtParam1 == null ? null : rtParam1.trim();
    }

    public String getRtParam2() {
        return rtParam2;
    }

    public void setRtParam2(String rtParam2) {
        this.rtParam2 = rtParam2 == null ? null : rtParam2.trim();
    }

    public String getRtParam3() {
        return rtParam3;
    }

    public void setRtParam3(String rtParam3) {
        this.rtParam3 = rtParam3 == null ? null : rtParam3.trim();
    }

    public String getRtParam4() {
        return rtParam4;
    }

    public void setRtParam4(String rtParam4) {
        this.rtParam4 = rtParam4 == null ? null : rtParam4.trim();
    }

    public String getRtParam5() {
        return rtParam5;
    }

    public void setRtParam5(String rtParam5) {
        this.rtParam5 = rtParam5 == null ? null : rtParam5.trim();
    }
}