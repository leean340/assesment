package com.test.service;

import com.google.common.base.Preconditions;
import com.test.bean.*;
import com.test.dao.*;
import com.test.helper.RequestResultHelper;
import com.test.mapper.CustomerBeanMapper;
import com.test.mapper.OrderBeanMapper;
import com.test.mapper.RoleBeanMapper;
import com.test.util.ErrorCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl {

    @Autowired
    private CustomerBeanMapper customerBeanMapper;

    @Autowired
    private OrderBeanMapper orderBeanMapper;

    @Autowired
    private RoleBeanMapper roleBeanMapper;

    @Autowired
    private UtilServiceImpl utilService;

    public RequestResult getCustomer(String idType, String idNumber) {
        RequestResult requestResult;
        String errorMessage;

        try {
            CustomerBeanExample customerBeanExample = new CustomerBeanExample();
            customerBeanExample.createCriteria().andCtIdTypeEqualTo(idType).andCtIdNumberEqualTo(idNumber);

            List<CustomerBean> customerBeanList = customerBeanMapper.selectByExample(customerBeanExample);
            Preconditions.checkArgument(customerBeanList.size() != 0, ErrorCode.LIST_IS_EMPTY);
            CustomerBean customerBean = customerBeanList.get(0);

            CustomerDAO customerDAO = new CustomerDAO();
            customerDAO.setId(Integer.toString(customerBean.getCtId()));

            IdentificatioinDAO identificatioinDAO = new IdentificatioinDAO();
            identificatioinDAO.setIdType(customerBean.getCtIdType());
            identificatioinDAO.setIdNumber(utilService.maskString(customerBean.getCtIdNumber()));

            customerDAO.setIdentificatioinDAO(identificatioinDAO);

            AddressDAO addressDAO = new AddressDAO();
            addressDAO.setId(customerBean.getCtAddressId());
            addressDAO.setPrimaryAddress(customerBean.getCtPrimaryaddress());
            addressDAO.setLine1(customerBean.getCtLine1());
            addressDAO.setLine2(customerBean.getCtLine2());
            addressDAO.setLine3(customerBean.getCtLine3());
            addressDAO.setPostalCode(customerBean.getCtPostalcode());
            addressDAO.setCity(customerBean.getCtCity());
            addressDAO.setState(customerBean.getCtState());
            addressDAO.setCountry(customerBean.getCtCountry());

            List<AddressDAO> addressDAOList = new ArrayList<>();
            addressDAOList.add(addressDAO);

            DetailsDAO detailsDAO = new DetailsDAO();
            detailsDAO.setSalutation(customerBean.getCtParam1());
            detailsDAO.setDisplayName(customerBean.getCtDisplayName());
            detailsDAO.setGender(customerBean.getCtParam2());
            detailsDAO.setDateOfBirth(customerBean.getCtDob());
            detailsDAO.setEmail(customerBean.getCtEmail());
            detailsDAO.setNationality(customerBean.getCtNationality());
            detailsDAO.setRace(customerBean.getCtRace());
            detailsDAO.setAddressDAOList(addressDAOList);

            customerDAO.setDetailsDAO(detailsDAO);


            List<ContactDAO> contactDAOList = new ArrayList<>();
            for (int i = 0; i < 2; i++) {
                ContactDAO contactDAO = new ContactDAO();
                if (i == 0) {
                    contactDAO.setId(customerBean.getCtContactId());
                    contactDAO.setType(customerBean.getCtContactType());
                    contactDAO.setValue(customerBean.getCtContactValue());

                } else {
                    int secondid = Integer.parseInt(customerBean.getCtContactId()) + 1;
                    contactDAO.setId(Integer.toString(secondid));
                    contactDAO.setType(customerBean.getCtParam3());
                    contactDAO.setValue(customerBean.getCtEmail());

                }
                contactDAOList.add(contactDAO);
            }

            customerDAO.setContactDAOList(contactDAOList);

            RoleBeanExample roleBeanExample = new RoleBeanExample();
            roleBeanExample.createCriteria().andRtCtIdEqualTo(customerBean.getCtId());

            List<RoleBean> roleBeanList = roleBeanMapper.selectByExample(roleBeanExample);
            List<RolesDAO> rolesDAOList = new ArrayList<>();

            for (RoleBean roleBean : roleBeanList) {
                RolesDAO rolesDAO = new RolesDAO();
                rolesDAO.setCode(roleBean.getRtCode());
                rolesDAO.setDescription(roleBean.getRtDescription());

                rolesDAOList.add(rolesDAO);
            }

            customerDAO.setRolesDAOList(rolesDAOList);

            MetaCustomerDAO metaCustomerDAO = new MetaCustomerDAO();
            metaCustomerDAO.setCode("200");

            requestResult = RequestResultHelper.standardizeWithObject(customerDAO, metaCustomerDAO);
        } catch (Exception e) {
            errorMessage = e.getMessage();
            requestResult = RequestResultHelper.standardize(false, errorMessage);
        }
        return requestResult;
    }

    public RequestResult getOrder(String customerId, String startDate, String endDate, String statusCode, String startRecord, String recordLimit) {
        RequestResult requestResult;
        String errorMessage;

        try {
            OrderBeanExample orderBeanExample = new OrderBeanExample();
            orderBeanExample.createCriteria().andOtParam1EqualTo(customerId);

            List<OrderBean> orderBeanList = orderBeanMapper.selectByExample(orderBeanExample);
            Preconditions.checkArgument(orderBeanList.size() != 0, ErrorCode.LIST_IS_EMPTY);

            List<TransactionDAO> transactionDAOList = new ArrayList<>();

            for (OrderBean orderBean : orderBeanList) {
                TransactionDAO transactionDAO = new TransactionDAO();

                if ((orderBean.getOtCreatedDatetime() != null && orderBean.getOtCreatedDatetime().equals(startDate)) || (orderBean.getOtCreatedDatetime() != null && orderBean.getOtCreatedDatetime().equals(endDate)) || (orderBean.getOtStatus() != null && orderBean.getOtStatus().equals(statusCode))) {
                    transactionDAO.setTrxRefId(orderBean.getOtTrxRefId());
                    transactionDAO.setStatus(orderBean.getOtStatus());
                    transactionDAO.setStatusDesc(orderBean.getOtStatusDesc());
                    transactionDAO.setRemarks(orderBean.getOtRemarks());
                    transactionDAO.setCreated(orderBean.getOtCreatedDatetime());
                    transactionDAO.setLastUpdated(orderBean.getOtUpdatedDatetime());
                } else {
                    transactionDAO.setTrxRefId(orderBean.getOtTrxRefId());
                    transactionDAO.setStatus(orderBean.getOtStatus());
                    transactionDAO.setStatusDesc(orderBean.getOtStatusDesc());
                    transactionDAO.setRemarks(orderBean.getOtRemarks());
                    transactionDAO.setCreated(orderBean.getOtCreatedDatetime());
                    transactionDAO.setLastUpdated(orderBean.getOtUpdatedDatetime());
                }
                transactionDAOList.add(transactionDAO);
            }
            MetaOrderDAO metaOrderDAO = new MetaOrderDAO();
            metaOrderDAO.setCode("200");
            metaOrderDAO.setReadCount(transactionDAOList.size() + "");
            requestResult = RequestResultHelper.standardizeWithObject(transactionDAOList, metaOrderDAO);
        } catch (Exception e) {
            errorMessage = e.getMessage();
            requestResult = RequestResultHelper.standardize(false, errorMessage);
        }
        return requestResult;
    }
}
