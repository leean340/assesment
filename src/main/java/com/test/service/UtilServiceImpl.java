package com.test.service;

import org.springframework.stereotype.Service;

@Service
public class UtilServiceImpl {

    public String maskString(String id) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < id.length(); i++)
            if (i < (id.length() - 4)){
                sb.append("*");}
        return sb.toString()+id.substring(id.length()-4);
    }
}
