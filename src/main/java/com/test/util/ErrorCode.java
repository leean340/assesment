package com.test.util;

import org.springframework.stereotype.Service;

@Service
public class ErrorCode {
    public static final String ERROR_CODE = "0";
    public static final String ERROR_CODE_MSG = "ERROR";

    public static final String LIST_IS_EMPTY = "101001";

}
